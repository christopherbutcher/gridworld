﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
                                                    // Christopher Butcher, 2021.
public class TileScript : MonoBehaviour
{
    public int terrainType;                         // The terrain type of this tile.
                                                    
    public float cost;                              // The cost of the tile
    public int occupied = 0;                        // The number of characters on this tile.

    public Vector3 locationPlusOffset;              // The location of this particular tile,
                                                    //  adjusted with the offset.

    public List<GameObject> adjacentTiles =         // List of all adjacent tiles.
        new List<GameObject>();

    static int num;                                 // Used to assign unique name to each tile.

    int colorInt = 0;                               // Used to change color over time.

    SpriteRenderer spriteRenderer;                  // The tile's SpriteRenderer component.
    TileHandler handler;                            // The TileHandler of the scene.

    public bool isHouse = false;                    // Determines if this Tile is part of a House.
    public bool isMine  = false;                    // Determines if this Tile is a Mine.

    private void Start()
    {
        name = "TILE " + num;                       // Gives each tile a unique name.
        num++;                                      // Increases number of tiles.
        spriteRenderer =                            // Stores reference to SpriteRenderer.
            GetComponent<SpriteRenderer>();
        handler =                                   // Stores reference to TileHandler.
            FindObjectOfType<TileHandler>();
    }

    public void SetTileColor(Color c)
    {
        if (!isHouse)
        {
            colorInt = 0;                           // Resets colorInt.
            spriteRenderer.color = new Color(       // Sets the color of this sprite to
                c.a, c.g, c.b, c.a * 0.75f);        //  the color of the sheep using it.
        }
    }

    public void DecreaseAlpha(int slice)
    {
        if (!isHouse)
        {
            if (colorInt < 10)                      // Keeps track of times used.
            {
                float alph =                        // Holds the new alpha of the sprite.
                    spriteRenderer.color.a * 0.9f;
                spriteRenderer.color = new Color(   // Decreases the alpha of the sprite.
                    spriteRenderer.color.r,
                    spriteRenderer.color.g,
                    spriteRenderer.color.b,
                    alph);
            }
            else
            {
                spriteRenderer.color =              // Set color to clear.
                    new Color(1, 1, 1, 0);
            }
        }
    }

    public void DecreaseGrass()
    {
        int i = (int)transform.position.x;          // Finds the Tile's location
        int j = (int)transform.position.y;          //  inside the 2D array.
        terrainType--;                              // Update terrain type.
        cost = TileHandler.costs[terrainType];      // Update cost.
        handler.tm.SetTile(new Vector3Int(i, j, 0), // Update sprite.
            handler.sprites[terrainType]);
    }

    public void DecreaseWater()
    {
        int k = (int)transform.position.x;          // Finds the Tile's location
        int l = (int)transform.position.y;          //  inside the 2D array.

        if (terrainType == 11)
        {
            terrainType--;                          // Decreases water.
        }
        else if(terrainType == 10)
        {
            terrainType = 0;                        // Turns water into dirt.
        }

        cost = TileHandler.costs[terrainType];      // Update cost.
        handler.tm.SetTile(new Vector3Int(k, l, 0), // Update sprite.
            handler.sprites[terrainType]);
    }

    public float GetWolfDistances()                 // Determines average distance from
    {                                               //  each Wolf to this tile.
        float result = 0;                           // Instantiates result.

        foreach(GameObject wolf in WorldHandler.wolves)
        {
            result +=                               // Adds distance of each Wolf.
                Vector2.Distance(wolf.transform.position, this.transform.position);
        }

        if(WorldHandler.wolves.Count > 0)
        {
            result =                                // Divides distance by number of Wolves.
                result / (WorldHandler.wolves.Count * 1.0f);
        }
        else
        {
            result = 17.5f;                         // Sets distance to maximum if no Wolves.
        }

        return result;                              // Returns average distance.
    }

    public float GetSheepDistances()                // Determines average distance from
    {                                               //  each Sheep to this tile.
        float result = 0;                           // Instantiates result.

        foreach (GameObject sheep in WorldHandler.sheeps)
        {
            result +=                               // Adds distances of Sheep.
                Vector2.Distance(sheep.transform.position, this.transform.position);
        }

        if (WorldHandler.sheeps.Count > 0)
        {
            result =                                // Divides distance by total 
                result / WorldHandler.sheeps.Count; //  number of Sheep.
        }
        else
        {
            result = 0;                             // Returns zero if no Sheep.
        }

        return result;                              // Returns average distance of Sheep.
    }

    public void ChangeTile(int type)                // Updates this tile.
    {
        if (!isHouse)
        {
            this.terrainType = type;                // Changes terrain type value.
            handler.tm.SetTile(new Vector3Int(      // Updates sprite.
                (int)locationPlusOffset.x,
                (int)locationPlusOffset.y, 0),
                handler.sprites[type]);
            this.cost = TileHandler.costs[type];    // Updates cost.
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if( collision.gameObject.tag == "Sheep" ||
            collision.gameObject.tag == "Wolf")
        {
            occupied++;                             // Adds to occupied number.
            //Debug.Log("@_@ OTE" + name + " " + occupied + " " + collision.gameObject.name);
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Sheep" ||
            collision.gameObject.tag == "Wolf")
        {
            if (occupied <= 0)
            {
                occupied = 1;                       // Corrects if spawned on tile.
                //Debug.Log("@_@ OTS" + name + " " + occupied + " " + collision.gameObject.name);
            }
        }  
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Sheep" ||
            collision.gameObject.tag == "Wolf")
        {
            if (occupied > 1)
            {
                occupied--;                         // Decreases occupied number.
                //Debug.Log("@_@ OTX 1" + name + " " + occupied + " " + collision.gameObject.name);
            }
            else
            {
                occupied = 0;
                //Debug.Log("@_@ OTX 2" + name + " " + occupied + " " + collision.gameObject.name);
            }
        }
    }
}
