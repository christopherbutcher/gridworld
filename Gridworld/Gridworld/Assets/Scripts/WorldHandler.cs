﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
                                                                // Christopher Butcher, 2021.
public class WorldHandler : MonoBehaviour
{
    public static List<GameObject>                              // All Sheep  in the scene.
        sheeps = new List<GameObject>();

    public static List<GameObject>                              // All Wolves in the scene.
        wolves = new List<GameObject>();

    public static List<House>                                   // All House scripts in scene.
        houses = new List<House>();

    public static List<Mine>                                    // All Mine  scripts in scene.
        mines = new List<Mine>();

    public GameObject sheepPrefab;                              // The Sheep prefab.
    public GameObject wolfPrefab;                               // The Wolf  prefab.

    int makeSheep  = 3;                                         // Sheep  to make at start of level.
    int makeWolves = 3;                                         // Wolves to make at start of level.

    public SheepScript currentSheep = null;                     // Currently selected SheepScript.

    float offset;                                               // The offset of 0.5f.

    public static Dictionary<string, bool> worldConditions
        = new Dictionary<string, bool>()
        {
            {"BUILD", false}, {"FULL" , false}, {"HOME", false}, 
            {"MINE" , false}, {"ROCKS", true }
        };

    public static Planner plan;

    public static bool checkingConditions;
    public static bool updatingConditions;

    void Start()
    {
        houses.Clear();                                         // Clear all Dictionaries
        mines.Clear();                                          //  after restart.
        sheeps.Clear();
        wolves.Clear();
        
        offset = SheepScript.offset;                            // Takes in the offset &
        int minX = (int)(SheepScript.min_X - offset);           //  min/max values from
        int maxX = (int)(SheepScript.max_X + offset);           //  SheepScript in order
        int minY = (int)(SheepScript.min_Y - offset);           //  to instantiate a
        int maxY = (int)(SheepScript.max_Y + offset);           //  number of Sheep.

        for(int s = 0; s < makeSheep; s++)                      // Creates the Sheep.
        {   
            Instantiate(sheepPrefab,
                new Vector3(Random.Range(minX, maxX) + offset,
                Random.Range(minY, maxY) + offset, 0),
                Quaternion.identity);
        }
        for(int w = 0; w < makeWolves; w++)                     // Creates the Wolves.
        {
            Instantiate(wolfPrefab,
                new Vector3(Random.Range(minX, maxX) + offset,
                Random.Range(minY, maxY) + offset, 0),
                Quaternion.identity);
        }

        plan = GetComponent<Planner>();
    }

    

    void Update()
    {
        if(sheeps.Count > TileHandler.tileGrid.Length * 0.6f ||
            wolves.Count > TileHandler.tileGrid.Length * 0.6f)
        {                                                       // If ever the number of Sheep
            sheeps.Clear();                                     //  or Wolves ever exceeds 2/3
            wolves.Clear();                                     //  the number of spaces, the
            SceneManager.LoadScene(0);                          //  game will reset.
        }                                                        

        if (Input.GetKeyDown(KeyCode.Escape))                   // Allows the user to
        {                                                       //  leave Gridworld.
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.Return))                   // Allows the user to
        {                                                       //  restart Gridworld.
            SceneManager.LoadScene(0);
        }

        if (Input.GetMouseButtonDown(0))                        // On left click:
        {
            Vector3 mousePos =                                  // Get mouse position.
                Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D[] hits =                               // Raycast downward.
                Physics2D.RaycastAll(mousePos, Vector2.zero);

            foreach(RaycastHit2D hit in hits)                   // Checks all possible hits.
            {
                if (hit.collider != null)                       // If Raycast hit something:
                {
                    //Debug.Log("Hit: " + hit.collider.name);
                    if (hit.collider.CompareTag("Sheep"))       // If Raycast hit a Sheep:
                    {
                        currentSheep = null;                    // Resets the currentSheep.

                        currentSheep =                          // Make sheep the currentSheep.
                            hit.collider.GetComponent<SheepScript>();
                        break;                                  // So Sheep will not move to
                    }                                           //  their current tile & stop.
                    else
                    {
                        if (currentSheep != null)               // If there is a currentSheep:
                        {
                            currentSheep.moving = false;        // Stops the sheep's old path.

                            int destX =                         // Translate the x-coordinate
                                (int)(mousePos.x);              //  into an integer.
                            int destY =                         // Translate the y-coordinate
                                (int)(mousePos.y);              //  into an integer.

                            GameObject ending =                 // Find the corresponding tile
                                TileHandler.tileGrid[           //  for the ending.
                                    destX, destY].gameObject;

                            GameObject beginning =               // Find the corresponding tile
                                TileHandler.tileGrid[            //  for the beginning.
                                    (int)(currentSheep.GetComponent<
                                        SheepScript>().currentPos.x - offset),
                                    (int)(currentSheep.GetComponent<
                                        SheepScript>().currentPos.y - offset)].gameObject;

                            if (!currentSheep.searching)
                            {
                                currentSheep.PathfindingWithAStar(
                                        beginning, ending);     // Start A* Pathfinding!
                            }
                            
                            currentSheep = null;                // Resets currentSheep.
                        }
                    }
                }
            }
        }
    }

    public static void CheckWorldState()
    {
        Debug.Log("GOAP!: Updating World States.");
        //Debug.Log("*GOAP: WORLD STATE PRE: " + worldConditions.Count);
        int rocks = 0;
        foreach(GameObject g in TileHandler.tileGrid)
        {
            if(g.GetComponent<TileScript>().terrainType == 12)
            {
                rocks++;
            }
        }

        if(rocks > 0)
        {
            worldConditions["ROCKS"] = true;
        }
        else
        {
            worldConditions["ROCKS"] = false;
        }

        bool roomsAvailable = false;
        foreach (House h in houses)
        {
            if(h.occupancy < House.MAX_OCCUPANCY)
            {
                roomsAvailable = true;
            }
        }
        worldConditions["HOME"] = roomsAvailable;

        if (mines.Count <= 0)
        {
            worldConditions["MINE"] = false;
        }
        else
        {
            worldConditions["MINE"] = true;
        }
        //Debug.Log("*GOAP: WORLD STATE POST: " + worldConditions.Count);
    }
}
