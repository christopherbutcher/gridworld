﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
                                    // Christopher Butcher, 2021.
public class TileNode
{
    public float gCost;             // Movement cost to get from start to here.
    public float hCost;             // Movement cost to get from here  to  end.
    public float fCost;             // Total    cost to get from start to  end.

    public GameObject parentNode;   // GameObject corresponding to the parent TileNode.

    public Dictionary<string, float> tileInfo = new Dictionary<string, float>()
    {                               // Dictionary storing information about the TileNode.
        {"GRASS", 0 }, {"SHEEP", 0}, {"TRAPS", 0 }, {"WATER", 0}, {"WOLVES", 0}
    };

    public void CalculateFCost()    // Calculates total cost.
    {
        fCost = gCost + hCost;
    }
}
