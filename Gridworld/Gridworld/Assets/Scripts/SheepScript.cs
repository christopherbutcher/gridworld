﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
                                                                // Christopher Butcher, 2021.
public class SheepScript : MonoBehaviour
{
    public static float min_X =  0.5f;                          // Keeping track of the
    public static float min_Y =  0.5f;                          //  bounds of Gridworld,
    public static float max_X = 15.5f;                          //  in case the bounds
    public static float max_Y =  9.5f;                          //  change.

    public static float offset = 0.5f;                          // Offset for Sheep position.

    static int count = 0;                                       // Counter for naming Sheep.

    public float hp = 12.0f;                                    // Current health & health
                                                                //  threshold for making
    float healthThreshold = 8.0f;                               //  new Sheep.

    static float MAX_HP = 12.0f;                                // Maximum HP for Sheep.

    float thirst = 1f;                                          // Current thirst of the Sheep.
    static float MAX_THIRST = 10f;                              // Maximum amount for thirst.

    public GameObject sheepPrefab;                              // Sheep prefab.

    public Vector3 currentPos;                                  // Current position.
    public Vector3 nextPos;                                     // Next position.
    public Vector3 destination;                                 // Destination.

    float timer = 0f;                                           // Timer.
    float timerThreshold = 10f;                                 // Timer threshold.

    public List<GameObject> pathway  = new List<GameObject>();  // The path for the Sheep to take.
    public List<GameObject> openList = new List<GameObject>();  // The possible moves for the Sheep.
    public List<GameObject> closed   = new List<GameObject>();  // The closed list of tiles.   
    public List<GameObject> crumbs   = new List<GameObject>();  // The path already taken.

    Dictionary<GameObject, TileNode> distances =                // Holds all TileNodes for all tiles.
        new Dictionary<GameObject, TileNode>();

    public bool moving;                                         // True if Sheep is moving.
    public bool searching;                                      // True if Sheep is searching for
                                                                //  their next move.

    public float speed = 1f;                                    // Walking speed of the Sheep.

    public Color sheepTint = new Color(0, 0, 0, 1);             // The color of this Sheep.

    public enum Actions                                         // Actions available to Sheep.
    {
        Thinking, Eating, Drinking, BeingMerry, Roaming, GOAPing
    }

    Actions currentAction = Actions.Thinking;                   // Instantiates current action.

    Dictionary<string, float> sheepChoices =                    // Dictionary to keep track of
        new Dictionary<string, float>()                         //  possible actions & utility
    {                                                           //  AI values.
        {"DRINK", 0}, {"EAT", 0}, {"GOAP", 0.3f}, {"MERRY", 0}, {"RUN", 0}
    };

    public Dictionary<string, bool> GOAPforSHEEP =              // Dictionary used to keep track
        new Dictionary<string, bool>()                          //  of states for goal-oriented
    {                                                           //  action planning (GOAP).
        {"HAMMER", false}, {"MATERIAL", false},  {"PICK", false}, {"SAFE", false}
    };

    public GoapAction currentGoapAction = null;                 // The current GOAP Action.

    void Start()
    {
        name = "SHEEP " + count;                                // Names the Sheep.
        count++;                                                // Increments counter.
        WorldHandler.sheeps.Add(this.gameObject);               // Add to the list of Sheep in scene.
        timerThreshold = Random.Range(8f, 12f);                 // Randomizes timerThreshold.

        sheepTint =                                             // Color coordinate the Sheep.
            new Color(Random.Range(0.1f, 1), 
            Random.Range(0.1f, 1),
            Random.Range(0.1f, 1), 1);                          

        GetComponent<SpriteRenderer>().color = sheepTint;       // Random tint for each Sheep (for fun!).

        currentPos  = transform.position;                       // Keeps track of the current position.
        nextPos     = transform.position;                       // Initializes next  position as current.
        destination = transform.position;                       // Initializes final position as current.

        hp     = Random.Range(MAX_HP     / 2, MAX_HP    );      // Randomizes starting health.
        thirst = Random.Range(MAX_THIRST / 2, MAX_THIRST);      // Randomizes starting thirst.

        foreach (GameObject tile in TileHandler.tileGrid)       // Instantiates all of the tiles
        {                                                       //  with a corresponding TileNode.
            distances.Add(tile, new TileNode());                // Makes a new TileNode instance.
        }
    }

    void Update()
    {
        if (currentPos.x % 0.5f != 0 ||                         // Handles cases where the
            currentPos.y % 0.5f != 0)                           //  currentPos is not _.5f.
        {
            currentPos = 
                new Vector3(Mathf.Round(currentPos.x) + offset,
                Mathf.Round(currentPos.y) + offset, currentPos.z);
        }

        if (nextPos.x % 0.5f != 0 ||                            // Handles cases where the
            nextPos.y % 0.5f != 0)                              //  nextPos is not _.5f.
        {
            nextPos =
                new Vector3(Mathf.Round(nextPos.x) + offset,
                Mathf.Round(nextPos.y) + offset, currentPos.z);
        }

        if(transform.position.x < min_X)                        // Handles cases where the
        {                                                       //  sheep wander off the map.
            transform.position = 
                new Vector3(min_X, transform.position.y, 0);
        }

        if(transform.position.x > max_X)
        {
            transform.position = 
                new Vector3(max_X, transform.position.y, 0);
        }

        if (transform.position.y < min_Y)
        {
            transform.position = 
                new Vector3(transform.position.x, min_Y, 0);
        }

        if (transform.position.y > max_Y)
        {
            transform.position = 
                new Vector3(transform.position.x, max_Y, 0);
        }

        if (!searching)                                         // If not searching for path.
        {
            if (pathway.Count > 0)                              // If there is a path,
            {
                speed = 1 / pathway[                            // Set current speed to
                    pathway.Count - 1].GetComponent<            //  reflect current tile.
                        TileScript>().cost;

                if (Vector3.Distance(transform.position,
                    pathway[pathway.Count-1].transform.position) > 0.1f)
                {
                    transform.position = Vector3.Lerp(          // Move the Sheep toward the
                        transform.position,                     //  current waypoint.
                        pathway[pathway.Count-1].transform.position, 
                        0.05f * speed);
                }
                else
                {
                    currentPos =                                // Set currentPos to tile's Vector3.
                        pathway[pathway.Count-1].transform.position;
                    TriggerTrap();                              // Triggers a trap (if necessary).

                    crumbs.Add(pathway[pathway.Count - 1]);     // Add tile to crumbs for colorization.
                    transform.position = currentPos;            // Make sure position is (_.5, _.5, 0).
                    pathway.Remove(pathway[pathway.Count-1]);   // Remove tile from end of pathway list.

                    if(pathway.Count > 1)                       // Checks if path is blocked.
                    {
                        if (pathway[pathway.Count - 1].GetComponent<TileScript>().occupied > 2)
                        {
                            MakeDecision();                     // Makes new decision if path blocked.
                            Debug.Log(name + " has changed their mind!");
                        }
                        else                                    // Stops pathfinding if next to destination.
                        {
                            PathfindingWithAStar(pathway[0], pathway[0]);
                        }
                    }

                    foreach(GameObject g in crumbs)             // Set color of all tiles in crumbs list.
                    {
                        g.GetComponent<TileScript>().DecreaseAlpha(crumbs.IndexOf(g));
                    }
                }
            }
            else
            {
                if (moving)
                {
                    speed = 1f;                                 // Reset speed.
                    Act();                                      // Perform designated action once.
                    moving = false;                             // Set moving to false if not moving.
                }
            }

            if(timer < timerThreshold)      
            {
                timer += Time.deltaTime;                        // Increases time.
            }
            else
            {
                MakeDecision();                                 // Makes decision once time is met.
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)      // Decides again if colliding with
    {                                                           //  another Sheep (or anything).
        MakeDecision();
    }

    private void OnTriggerEnter2D(Collider2D collision)         // If close to another Sheep,
    {                                                           //  calls CreateSheep() method.
        if (collision.gameObject.tag == "Sheep")
        {
            //Debug.Log(name + " & " + collision.gameObject.name + " are close!");
            if(hp >= healthThreshold && currentAction == Actions.BeingMerry)
            {
                //Debug.Log("New sheep time!");
                CreateSheep();                                  // Makes new Sheep if applicable.
                currentAction = Actions.Thinking;               // Resets current action to
            }                                                   //  prevent multiple calls.
        }
    }

    private void EatGrass()
    {
        TileScript thisTile = TileHandler.tileGrid[             // Keeps track of the tile
            (int)(transform.position.x - offset),               //  that the sheep is
            (int)(transform.position.y - offset)].              //  currently on.
            GetComponent<TileScript>();

        if (transform.position.x >= min_X &&                    // Determines if sheep is
            transform.position.x <= max_X &&                    //  within level bounds &
            transform.position.y >= min_Y &&                    //  that the tile has grass.
            transform.position.y <= max_Y &&
            thisTile.terrainType <= 5)
        {
            Debug.Log(name + " is eating grass at " + thisTile.name + ".");
            hp += (thisTile.terrainType * 0.05f);               // Increases health by the
            if(hp > MAX_HP)                                     //  height of the current
            {                                                   //  grass tile (capped at
                hp = MAX_HP;                                    //  MAX_HP).
            }
                
                                                                
            if(thisTile.terrainType > 0)
            {                                                   // Subtracts one from the
                thisTile.DecreaseGrass();                       //  height of the current
            }                                                   //  grass tile (if possible).
        }
    }

    public void CreateSheep()
    {
        GameObject baby = Instantiate(sheepPrefab,              // Creates a new sheep at this
            currentPos, Quaternion.identity);                   //  sheep's proper location.
        baby.GetComponent<SheepScript>().ChooseNextMove();      // Selects new sheep's next move.
        baby.GetComponent<SheepScript>().hp = hp * 0.25f;       // Gives the sheep health.
        hp *= 0.5f;                                             // Halves this sheep's current health.
        Debug.Log(name + " is the new parent of " + baby.name + " at " + currentPos);
    }


    public void ChooseNextMove()                                // Picks the next position for this sheep.
    {
        int xRandom = 0;                                        // initialization before randomization.
        int yRandom = 0;

        //------------------x------------------//
        if (transform.position.x <= min_X)                      // Search for next possible location,
        {                                                       //  adjusted for edge cases.
            xRandom = Random.Range(0, 2);
        }
        else if (transform.position.x >= max_X)
        {
            xRandom = Random.Range(-1, 1);
        }
        else
        {
            xRandom = Random.Range(-1, 2);
        }
        //------------------y------------------//
        if (transform.position.y <= min_Y)
        {
            yRandom = Random.Range(0, 2);
        }
        else if (transform.position.y >= max_Y)
        {
            yRandom = Random.Range(-1, 1);
        }
        else
        {
            yRandom = Random.Range(-1, 2);
        }

        nextPos = new Vector3(transform.position.x + xRandom,   // Update the next move.
                transform.position.y + yRandom,
                transform.position.z);
    }

    public void PathfindingWithAStar(GameObject startTile, GameObject endTile)
    {                                                           // A* Pathfinding!
        //Debug.Log("Pathfinding has begun from " + startTile + " to " + endTile);
        bool goalFound = false;                                 // Boolean local to this iteration.

        if (!moving)                                            // Does not compute if moving (no
        {                                                       //  chance of double click).                        
            openList.Clear();                                   // Clear all of the lists &
            pathway.Clear();                                    //  dictionaries for this
            closed.Clear();                                     //  sheep.

            foreach (GameObject g in crumbs)
            {
                g.GetComponent<TileScript>().                   // Resets color of tiles.
                    SetTileColor(new Color(1, 1, 1, 0));
            }
            crumbs.Clear();                                     // Clears crumbs list.

            foreach(GameObject tile in TileHandler.tileGrid)    // Updates all of the data for each 
            {                                                   //  tiles' corresponding TileNode.
                if (!distances.ContainsKey(tile))
                {
                    distances.Add(tile, new TileNode());        // Makes a new TileNode if necessary.
                }
                
                distances[tile].gCost = int.MaxValue;           // Sets cost to "infinity".

                distances[tile].hCost =                         // Calculates distance from goal.
                    Vector2.Distance(tile.transform.position,
                    endTile.transform.position);

                distances[tile].CalculateFCost();               // Calculates total cost.

                distances[tile].parentNode = null;              // Makes parent node 'null' to start.
            }

            TileNode currentNode = distances[startTile];        // Makes startTile's TileNode currentNode.
            currentNode.gCost =                                 // Sets travel cost to (almost) zero.
                (startTile.GetComponent<TileScript>().cost / 31f);

            currentNode.hCost =                                 // Calculates distance to destination.
                Vector2.Distance(startTile.transform.position,
                endTile.transform.position);

            currentNode.CalculateFCost();                       // Calculates total cost.

            GameObject currentTile = startTile;                 // Instantiates currentTile as startTile.
            openList.Add(currentTile);                          // Adds current tile to the open list.

            while(openList.Count > 0)
            {
                //Debug.Log("OPENLIST.COUNT: " + openList.Count);

                GameObject lowestCost = null;                   // Instantiates lowest-cost tile.

                foreach(GameObject g in openList)               // Finds lowest-cost tile in open list.
                {
                    distances[g].CalculateFCost();              // Recalculates total cost (just in case).

                    if(lowestCost != null)                      // If there's already a lowest-cost tile,
                    {                                           //  determine if this tile's cost is lower.
                        if(distances[g].fCost < distances[lowestCost].fCost)
                        {
                            lowestCost = g;                     // Make this tile the lowest-cost tile.
                        }
                    }
                    else
                    {
                        lowestCost = g;                         // If no lowest-cost tile, make this one it.
                    }
                }
                currentTile = lowestCost;                       // Make current tile the lowest cost one.
                openList.Remove(currentTile);                   // Remove current tile from open list.
                //Debug.Log("CURRENT TILE: " + currentTile);

                foreach (GameObject adj in                      // Check all adjacent tiles.
                    currentTile.GetComponent<
                        TileScript>().adjacentTiles)
                {
                    if (!closed.Contains(adj))                  // Make sure this tile isn't already used
                    {                                           //  (stops stack overflow parenting loop).
                        distances[adj].parentNode =             // Make current tile the adjacent's parent.
                            currentTile;
                    }

                    if (adj == endTile)                         // Determines if we have found our end tile.
                    {
                        goalFound = true; 
                    }

                    distances[adj].gCost =                      // Sets the cost to be current cost +
                        distances[currentTile].gCost +          //  terrain costs divided by 20.
                        (adj.GetComponent<TileScript>().cost / 31f);
                    //Debug.Log(adj + " - G: " + distances[adj].gCost);

                    distances[adj].hCost =                      // Determines distance from goal.
                        Vector2.Distance(adj.transform.position,
                        endTile.transform.position);
                    //Debug.Log(adj + " - H: " + distances[adj].hCost);

                    distances[adj].CalculateFCost();            // Calculates total cost.
                    //Debug.Log(adj + " - F: " + distances[adj].fCost);
                        
                    bool inadequate = false;                    // True if this tile is not optimal.

                    foreach (GameObject candidate in openList)
                    {
                        distances[candidate].CalculateFCost();  // Recalculate total cost.

                        if (distances[candidate].fCost < distances[adj].fCost)
                        {
                            inadequate = true;                  // A better candidate exists.
                        }
                    }
                    foreach (GameObject candidate in closed)
                    {
                        distances[candidate].CalculateFCost();  // Recalculate total cost.

                        if (distances[candidate].fCost < distances[adj].fCost)
                        {
                            inadequate = true;                  // A better path exists.
                        }
                    }

                    if (!inadequate && !openList.Contains(adj)) 
                    {
                        openList.Add(adj);                      // If best candidate, add to open list.
                        //adj.GetComponent<TileScript>().SetTileColor(Color.magenta);
                    }
                }
                closed.Add(currentTile);                        // Add current tile to closed list.
                //currentTile.GetComponent<TileScript>().SetTileColor(Color.cyan);
                //Debug.Log("#CLOSED LIST LENGTH: " + closed.Count);
            }
            if (goalFound)                                      // If we have found the goal,
            {                                                   //  we retrace our steps to
                //Debug.Log("GOAL FOUND!");                     //  find the path.
                Retrace(endTile);
            }
            else
            {                                                   // If the goal was not found,
                Debug.Log("!!! NO GOAL FOUND !!!");             //  retrace from where we
                Retrace(currentTile);                           //  currently are.
            }
        }
    }

    private void Retrace(GameObject nextTile)                   // Method for retracing out steps.
    {
        if (distances[nextTile].parentNode != null)             // If this is not the starting tile:
        {
            //Debug.Log("Retracing from " + nextTile + " back to " + distances[nextTile].parentNode);
            pathway.Add(nextTile);                              // Add this tile to our path.
            nextTile.GetComponent<                              // Change color of tile (for fun!).
                TileScript>().SetTileColor(sheepTint);
            Retrace(distances[nextTile].parentNode);            // Run method from tile's parent.

        }
        else
        {                                                       // If this is the starting tile,
            moving = true;                                      //  set the two booleans for the
            searching = false;                                  //  Update method.
            //Debug.Log("Retrace complete. Starting movement!");
        }
    }

    public void MakeDecision()                                  // Method for making decisions.
    {
        timer = 0;                                              // Resets timer.
        currentGoapAction =                                     // Determines next GOAP Action.
            WorldHandler.plan.DetermineNextAction(this);        
        if (!moving)                                            // Runs only if Sheep is not moving.
        {
            searching = true;                                   // Sets searching to true.

            GameObject  here = TileHandler.tileGrid[            // Determines currently occupied tile.
                (int)(currentPos.x - offset),
                (int)(currentPos.y - offset)].gameObject;

            GameObject there = TileHandler.tileGrid[            // Instantiates destination.
                (int)(currentPos.x - offset),
                (int)(currentPos.y - offset)].gameObject;

            float fear =                                        // Determines danger level of this tile.
                here.GetComponent<TileScript>().GetWolfDistances();
            //Debug.Log("&&&&FEAR:" + fear);

            string currentDecision = "";                        // Instantiates current decision.

            sheepChoices["EAT"] =                               // Calculates utility AI values.
                (1 / (1 + Mathf.Pow(2.718f, (-2 * (hp - 8)))));
            sheepChoices["DRINK"] =
                (1 / (1 + Mathf.Pow(2.718f, (-1.5f * (thirst - 6)))));
            sheepChoices["MERRY"] =  
                (1 / (1 + Mathf.Pow(2.718f, (1.75f * (hp - (healthThreshold + 1))))));
            sheepChoices["RUN"] =
                (1 / (1 + Mathf.Pow(2.718f, (1.5f * (17.5f - fear - 13.5f)))));

            foreach (KeyValuePair<string, float> kvp in sheepChoices)
            {
                //Debug.Log("$$$" + kvp.Key + ", " + kvp.Value);
                if (currentDecision.Equals(""))                 // Determines the best choice
                {                                               //  based on lowest value in
                    currentDecision = kvp.Key;                  //  the utility AI dictionary.
                }
                else
                {
                    if (kvp.Value < sheepChoices[currentDecision])
                    {
                        currentDecision = kvp.Key;
                    }
                }
            }
            //Debug.Log("FINAL DECISION: " + currentDecision);
            switch (currentDecision)                            // Runs method based on best action.
            {
                case "EAT":
                    there = FindGrass();                        // Finds best and/or closest grass.
                    currentAction = Actions.Eating;
                    break;
                case "DRINK":
                    there = FindWater();                        // Finds best and/or closest water.
                    currentAction = Actions.Drinking;
                    break;
                case "GOAP":                                    // Determines best Tile for next
                    if(currentGoapAction != null)               //  GOAP Action.
                    {
                        there = currentGoapAction.FindLocationForGOAP(here);
                        currentAction = Actions.GOAPing;
                    }
                    break;
                case "MERRY":
                    there = FindSheep();                        // Finds nearest Sheep.
                    currentAction = Actions.BeingMerry;
                    break;
                case "RUN":
                    there = FindSafety();                       // Finds furthest tile from Wolves.
                    currentAction = Actions.Roaming;
                    break;
                default:
                    there =                                     // Finds random tile.
                        TileHandler.tileGrid[
                            Random.Range((int)(min_X - offset), (int)(max_X - offset)),
                            Random.Range((int)(min_Y - offset), (int)(max_Y - offset))].gameObject;

                    if(there.GetComponent<TileScript>().terrainType == 14)
                    {
                        there = null;                           // Attempts to keep Sheep from
                    }                                           //  ending on Spike Tile.

                    currentAction = Actions.Roaming;            // Sets currentAction.
                    break;
            }

            if(there == null)                                   // Safeguard in case of null.
            {
                there =                                         // Finds random tile.
                        TileHandler.tileGrid[
                            Random.Range((int)(min_X - offset), (int)(max_X - offset)),
                            Random.Range((int)(min_Y - offset), (int)(max_Y - offset))].gameObject;
                currentAction = Actions.Roaming;
            }

            PathfindingWithAStar(here, there);                  // Run Pathfinding method.
        }
        else
        {
            //Debug.Log("NOT MOVING.");
        }
    }

    private GameObject FindGrass()
    {
        //Debug.Log("FINDING GRASS");
        GameObject bestGrass = null;                            // Instantiate bestGrass GameObject

        foreach(KeyValuePair<GameObject,                        // Search through all tiles/nodes.
            TileNode> t in distances)
        {
            TileScript ts =                                     // Grab corresponding TileScript.
                t.Key.GetComponent<TileScript>();           
            TileNode tn = t.Value;                              // Grab corresponding TileNode.

            if (ts.terrainType > 0 && ts.terrainType <= 5)      // If this Tile is a Grass Tile.
            {
                if (ts.occupied <= 2)                           // Makes sure it is not occupied.
                {
                    tn.tileInfo["GRASS"] =                      // Calculates distance & length.
                        (ts.terrainType * -2f) + Vector2.Distance(
                            t.Key.transform.position, transform.position);

                    float danger = ts.GetWolfDistances();       // Calculates danger level.
                    tn.tileInfo["WOLVES"] = danger;             // Updates information on TileNode.

                    tn.tileInfo["GRASS"] =                      // Factors in danger level.
                        (tn.tileInfo["GRASS"] - danger);

                    if(bestGrass == null)                       // Determines bestGrass based on
                    {                                           //  distance, level of grass, &
                        bestGrass = t.Key;                      //  closeness of Wolves.
                    }
                    else
                    {
                        if(tn.tileInfo["GRASS"] < 
                            distances[bestGrass].tileInfo["GRASS"])
                        {
                            bestGrass = t.Key;
                        }
                    }
                }
            }
        }
        return bestGrass;                                       // Returns best grass tile.
    }
    
    private GameObject FindWater()
    {
        //Debug.Log("FINDING WATER");
        GameObject bestWater = null;                            // Instantiate bestWater GameObject

        foreach (KeyValuePair<GameObject,                       // Search through all tiles/nodes.
            TileNode> t in distances)
        {
            TileScript ts =                                     // Grab corresponding TileScript.
                t.Key.GetComponent<TileScript>();
            TileNode tn = t.Value;                              // Grab corresponding TileNode.

            if (ts.terrainType == 10 ||                         // If this Tile is a Water Tile.
                ts.terrainType == 11)                       
            {
                if (ts.occupied <= 2)                           // Makes sure it is not occupied.
                {
                    tn.tileInfo["WATER"] =                      // Calculates distance & length.
                        (ts.terrainType * -0.2f) + Vector2.Distance(
                            t.Key.transform.position, transform.position);

                    float danger = ts.GetWolfDistances();       // Calculates danger level.

                    tn.tileInfo["WATER"] =                      // Factors in danger level.
                        (tn.tileInfo["WATER"] - danger);

                    if (bestWater == null)                      // Determines bestWater based on
                    {                                           //  distance, level of water, &
                        bestWater = t.Key;                      //  closeness of Wolves.
                    }
                    else
                    {
                        if (tn.tileInfo["WATER"] <
                            distances[bestWater].tileInfo["WATER"])
                        {
                            bestWater = t.Key;
                        }
                    }
                }
            }
        }
        return bestWater;                                       // Returns best water tile.
    }
    
    private GameObject FindSheep()
    {
        //Debug.Log("FINDING SHEEP");
        GameObject bestFriends = null;                          // Instantiate bestFriends GameObject

        foreach (KeyValuePair<GameObject,                       // Search through all tiles/nodes.
            TileNode> t in distances)
        {
            TileScript ts =                                     // Grab corresponding TileScript.
                t.Key.GetComponent<TileScript>();
            TileNode tn = t.Value;                              // Grab corresponding TileNode.

            float danger = ts.GetWolfDistances();               // Calculates danger level.
            tn.tileInfo["WOLVES"] = danger;                     // Sets danger level for TileNode.
            tn.tileInfo["SHEEP"] =                              // Sets Sheep distance plus danger
                (ts.GetSheepDistances() - danger);              //  level for this TileNode.


            if (ts.occupied <= 2)                               // Makes sure it is not occupied.
            {
                if (bestFriends == null)                        // Determines bestGrass based on
                {                                               //  distance of Sheep & closeness
                    bestFriends = t.Key;                        //  of Wolves.
                }
                else
                {
                    if (tn.tileInfo["SHEEP"] <
                        distances[bestFriends].tileInfo["SHEEP"])
                    {
                        bestFriends = t.Key;
                    }
                }
            }
        }
        return bestFriends;                                     // Returns the best destination.
    }

    private GameObject FindSafety()
    {
        //Debug.Log("FINDING SAFETY");
        GameObject safestTile = null;                           // Instantiate safestTile GameObject

        foreach (KeyValuePair<GameObject,                       // Search through all tiles/nodes.
            TileNode> t in distances)
        {
            TileScript ts =                                     // Grab corresponding TileScript.
                t.Key.GetComponent<TileScript>();
            TileNode tn = t.Value;                              // Grab corresponding TileNode.

            tn.tileInfo["WOLVES"] = ts.GetWolfDistances();      // Sets danger level for TileNode.
            tn.tileInfo["WOLVES"] += ts.cost;                   // Adds tile cost to danger level.


            if (ts.occupied <= 2 && ts.terrainType != 14)       // Makes sure Tile is visitable.
            {
                if (safestTile == null)                         // Determines safestTile based on
                {                                               //  distance of Wolves.
                    safestTile = t.Key;
                }
                else
                {
                    if (tn.tileInfo["WOLVES"] >
                        distances[safestTile].tileInfo["WOLVES"])
                    {
                        safestTile = t.Key;
                    }
                }
            }
        }
        Debug.Log(name + " found the safest tile: " + safestTile.name + "!");
        return safestTile;                                      // Returns the safest destination.
    }

    void DrinkWater()
    {
        TileScript bevvy =                                      // Determine currently occupied tile.
            TileHandler.tileGrid[
                (int)(currentPos.x - offset),
                (int)(currentPos.y - offset)].GetComponent<TileScript>();

        Debug.Log(name + " is drinking water at " + bevvy.name + ".");

        if (bevvy.terrainType == 10)                            // Increase thirst value depending
        {                                                       //  on the level of water tile.
            thirst += (MAX_THIRST / 2.0f);
        }
        else if (bevvy.terrainType == 11)
        {
            thirst = MAX_THIRST;
        }
        else
        {
            //Debug.Log("THIS IS NOT WATER.");
            // This space left blank for
            //  adding slimed water later.
        }

        if(thirst > MAX_THIRST)                                 // Caps thirst value.
        {
            thirst = MAX_THIRST;
        }

    }

    private void TriggerTrap()                                  // Used to trigger traps.
    {
        TileScript trap =                                       // Determines currently occupied tile.
            TileHandler.tileGrid[
                (int)(currentPos.x - offset),
                (int)(currentPos.y - offset)].GetComponent<TileScript>();

        if(trap.terrainType == 14)                              // Determines if this tile is a trap.
        {
            trap.ChangeTile(13);                                // Updates tile.
            TakeDamage(1);                                      // Takes damage.
        }
        else
        {
            //Debug.Log(trap.locationPlusOffset + " is not a trap");
        }

    }

    public void TakeDamage(int dmg)                             // Method for taking damage.
    {
        Debug.Log(name + " took " + dmg + " damage at " + currentPos);
        hp -= dmg;                                              // Subtract parameter from health.
        if(hp <= 0)                                             // If below zero, call PassOut method.
        {
            PassOut();
        }
    }

    private void PassOut()                                      // Method for passing out.
    {
        TileScript grave =                                      // Determine currently (& final)
            TileHandler.tileGrid[                               //  occupied tile.
                (int)(currentPos.x - offset),
                (int)(currentPos.y - offset)].GetComponent<TileScript>();
        Debug.Log("Goodbye, " + name + " (@ " + grave.name +").");
        grave.ChangeTile(12);                                   // Update tile, cost, & sprite.
        foreach (GameObject g in crumbs)
        {
            g.GetComponent<TileScript>().                       // Resets color of tiles.
                SetTileColor(new Color(1, 1, 1, 0)); 
        }
        crumbs.Clear();                                         // Clears crumbs list.
        WorldHandler.sheeps.Remove(this.gameObject);            // Remove Sheep from list of Sheep.
        Destroy(this.gameObject);                               // Destroy Sheep GameObject.
    }

    public void GoHome()                                        // For when a Sheep enters a House.
    {
        foreach (GameObject g in crumbs)
        {
            g.GetComponent<TileScript>().                       // Resets color of tiles.
                SetTileColor(new Color(1, 1, 1, 0));
        }
        crumbs.Clear();                                         // Clears crumbs list.
        WorldHandler.sheeps.Remove(this.gameObject);            // Remove Sheep from list of Sheep.
        Destroy(this.gameObject);                               // Destroy Sheep GameObject.
    }

    private void Act()                                          // Method for fulfilling an action.
    {
        //Debug.Log(name + " is ACTING.");
        if(thirst > 0.5f)                                       // Subtract from thirst.
        {                                                       
            thirst -= 0.5f;
        }
        else
        {
            thirst = 0;

            if(hp > 1f)                                         // Subtract from health (if necessary).
            {
                hp -= 1f;
            }
            else
            {
                PassOut();                                      // Pass out (if necessary).
            }
        }

        switch (currentAction)                                  // Call method & complete action.
        {
            case Actions.Eating:
                EatGrass();                                     // Eat grass.
                break;
            case Actions.Drinking:
                DrinkWater();                                   // Drink water.
                break;
            case Actions.GOAPing:                               // Performs next designated
                currentGoapAction.PerformAction(this);          //  GOAP Action.
                currentGoapAction = null;
                break;
            default:
                break;
        }

        currentAction = Actions.Thinking;                       // Reset current action.
    }
}
