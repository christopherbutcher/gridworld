﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.Tilemaps;
                                                                    // Christopher Butcher, 2021.
public class TileHandler : MonoBehaviour
{
    public static GameObject[,] tileGrid = new GameObject[16, 10];  // 2D array of tile GameObjects.

    public List<Tile> sprites = new List<Tile>();                   // List of possible tile sprites.

    //  0   : Dirt              //  5   : Grass (Lv. 5)     //  10  : Water (Lv. 1)  
    //  1   : Grass (Lv. 1)     //  6   : Sand  (Lv. 1)     //  11  : Water (Lv. 2)    
    //  2   : Grass (Lv. 2)     //  7   : Sand  (Lv. 2)     //  12  : Rock
    //  3   : Grass (Lv. 3)     //  8   : Sand  (Lv. 3)     //  13  : Trap  (Disarmed)
    //  4   : Grass (Lv. 4)     //  9   : Sand  (Lv. 4)     //  14  : Trap  (Armed)
                                                                    
    public static List<float> costs = new List<float>               // List of possible tile costs.
    {
        1.10f, 1.00f, 1.25f, 1.50f, 1.75f, 
        2.00f, 2.50f, 2.75f, 3.50f, 4.00f,
        8.25f, 12.5f, 22.2f, 0.75f, 31.0f
    };

    public GameObject tilePrefab;                                   // Prefab of a tile.

    public Tilemap tm;                                              // The scene's tilemap.

    static float grassTimer = 0f;                                   // Timer for growing grass.

    public static float grassThreshold = 10f;                       // The threshold for the timer.

    public static float offset = 0.5f;                              // The offset for tile position.

    public static bool setupCompleted;                              // True if setup is complete.

    void Start()
    {
        setupCompleted = false;                                     // Resets bool on reload.

        grassTimer = 0f;                                            // Set timer to zero.

        for (int i = 0; i < tileGrid.GetLength(0); i++)
        {
            for (int j = 0; j < tileGrid.GetLength(1); j++)
            {
                CreateTile(i, j);                                   // Initializes tiles.
            }
        }

        for (int i = 0; i < tileGrid.GetLength(0); i++)
        {
            for (int j = 0; j < tileGrid.GetLength(1); j++)
            {
                FindAdjacentTiles(i, j);                            // Finds adjacent tiles after
            }                                                       //  all have been created.
        }
        setupCompleted = true;                                      // Allows setup to continue.
    }

    void Update()
    {
        grassTimer += Time.deltaTime;                               // Increase the timer.
        if (grassTimer > grassThreshold)                            // Once the timer exceeds the
        {                                                           //  threshold, Gridworld resets
            grassTimer = 0;                                         //  the timer and calls the
            for (int i = 0; i < tileGrid.GetLength(0); i++)
            {
                for (int j = 0; j < tileGrid.GetLength(1); j++)
                {
                    if(tileGrid[i,j].GetComponent<
                        TileScript>().terrainType <= 5)             // Checks if a grass/dirt tile.
                    {
                        ChangeGrass(i, j);                          // Change grass amount.
                    }
                }
            }
        }
    }

    void CreateTile(int i, int j)
    {
        int rand = Random.Range(0, sprites.Count);                  // Randomly assigns tile type.

        GameObject g = Instantiate(tilePrefab, this.transform);     // Create prefab.
        TileScript t = g.GetComponent<TileScript>();                // Assign TileScript.
        t.terrainType = rand;                                       // Set terrainType.
        t.cost = costs[t.terrainType];                              // Set cost by terrainType.
        tm.SetTile(new Vector3Int(i, j, 0),                         // Set sprite by terrainType.
            sprites[t.terrainType]);
        t.locationPlusOffset =                                      // Set location by array index.
            new Vector3(i + offset, j + offset, 0);
        g.transform.position = t.locationPlusOffset;                // Place on grid.
        tileGrid[i, j] = g;                                         // Add to the 2D array.
    }

    void ChangeGrass(int i, int j)
    {
        TileScript t = tileGrid[i, j].GetComponent<TileScript>();   // Finds corresponding TileScript.

        if(t.terrainType > 0)                                       // If there is grass on this tile.
        {
            if (t.terrainType < 5)                                  // If this is a grass tile.
            {
                t.terrainType++;                                    // Update terrain type.
                t.cost = costs[t.terrainType];                      // Update cost.
                tm.SetTile(new Vector3Int(i, j, 0),                 // Update sprite.
                    sprites[t.terrainType]);
            }
            else if (t.terrainType == 5)
            {
                StartCoroutine(Wither(i, j));                       // Starts coroutine if grass is
                                                                    //  at max height.
            }
            else
            {
                Debug.LogError("Wrong terrain type @ "
                    + t.locationPlusOffset);
            }
        }
        else
        {
            int d20 = Random.Range(1, 21);                          // Rolls to see if tile without
            if (d20 >= 10)                                          //  grass can gain some grass.
            {
                t.terrainType++;                                    // Update terrain type.
                t.cost = costs[t.terrainType];                      // Update cost.
                tm.SetTile(new Vector3Int(i, j, 0),                 // Update sprite.
                    sprites[t.terrainType]);
            }                         
        }
    }

    private IEnumerator Wither(int i, int j)                        // Reduces grass height to zero.
    {
        TileScript t = tileGrid[i, j].GetComponent<TileScript>();   // Get TileScript from object.
        int d20 = Random.Range(1, 21);                              // Rolls to see if it takes effect.
        if (d20 < 10)
        {
            while (t.terrainType > 0)                               // Decreases grass amount on tile
            {                                                       //  & updates the sprite of the
                t.terrainType--;                                    //  tile until there is no more
                t.cost = costs[t.terrainType];                      // Update cost.
                tm.SetTile(new Vector3Int(i, j, 0),                 // Update sprite.
                    sprites[t.terrainType]);
                yield return new WaitForSecondsRealtime(0.66f);     // Wait.
            }
        }
        yield return null;
    }


    void FindAdjacentTiles(int i, int j)
    {
        TileScript current = 
            tileGrid[i, j].GetComponent<TileScript>();              // Chooses current TileScript.

        if(i - offset >= SheepScript.min_X)
        {
            current.adjacentTiles.Add(tileGrid[i - 1, j]);          // Adds W tile (if possible).

            if (j + offset < SheepScript.max_Y)
            {
                current.adjacentTiles.Add(tileGrid[i - 1, j + 1]);  // Adds NW tile (if possible).
            }
            if(j - offset >= SheepScript.min_Y)
            {
                current.adjacentTiles.Add(tileGrid[i - 1, j - 1]);  // Adds SW tile (if possible).
            }
        }

        if (i + offset < SheepScript.max_X)
        {

            current.adjacentTiles.Add(tileGrid[i + 1, j]);          // Adds E tile (if possible).

            if (j + offset < SheepScript.max_Y)
            {
                current.adjacentTiles.Add(tileGrid[i + 1, j + 1]);  // Adds NE tile (if possible).
            }
            if (j - offset >= SheepScript.min_Y)
            {   
                current.adjacentTiles.Add(tileGrid[i + 1, j - 1]);  // Adds SE tile (if possible).
            }
        }

        if(j + offset < SheepScript.max_Y)
        {
            current.adjacentTiles.Add(tileGrid[i, j + 1]);          // Adds N tile (if possible).
        }

        if(j - offset >= SheepScript.min_Y)
        {
            current.adjacentTiles.Add(tileGrid[i, j - 1]);          // Adds S tile (if possible).
        }
    }
}
