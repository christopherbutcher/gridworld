﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
                                                                // Christopher Butcher, 2021
public class WolfScript : MonoBehaviour
{
    static float min_X = 0.5f;                                  // Keeping track of the
    static float min_Y = 0.5f;                                  //  bounds of Gridworld,
    static float max_X = 15.5f;                                 //  in case the bounds
    static float max_Y = 9.5f;                                  //  change.

    static float offset = 0.5f;                                 // Offset for Wolf position.

    public float hp = 10f;                                      // Current health & threshold
    float hpThreshold = 8f;                                     //  for making new Wolves.
    static float MAX_HP = 10f;                                  // Maximum HP for Wolves.

    public GameObject wolfPrefab;                               // Wolf prefab.

    public Vector3 currentPosition;                             // Current position of Wolf.
    public Vector3 nextPosition;                                // Next    position of Wolf.
    public Vector3 destination;                                 // Final   position of Wolf.

    float timer = 0f;                                           // Timer.
    float timerThreshold = 12f;                                 // Timer threshold.

    public List<GameObject> wolfPath = new List<GameObject>();  // The pathway for the Wolf to take.
    public List<GameObject> openList = new List<GameObject>();  // Possible moves for the Wolf.
    public List<GameObject> closed   = new List<GameObject>();  // The closed list of Tiles.
    public List<GameObject> crumbs   = new List<GameObject>();  // The path already taken.

    Dictionary<GameObject, TileNode> wolfDistances =            // Holds all TileNodes for all Tiles.
        new Dictionary<GameObject, TileNode>();

    public bool moving;                                         // True if Wolf is moving.
    public bool searching;                                      // True if Wolf is searching.

    float speed = 0.1f;                                         // Walking speed of the Wolf.

    public Color wolfTint = new Color(0, 0, 0, 1);              // The color of the Wolf (for fun!).

    static int count = 0;                                       // Used for identifying Wolves.

    public enum WolfActions                                     // Enumerator of actions.
    {
        Thinking, Attacking, Absorbing, Splitting, Trapping
    }

    WolfActions currentAction = WolfActions.Thinking;           // Instantiate currentAction to Thinking.

    Dictionary<string, float> wolfChoices =                     // Dictionary to keep track of values
        new Dictionary<string, float>()                         //  for use with utility AI.
    {
        {"ATTACK", 0}, {"DRINK", 0}, {"SPLIT", 0}, {"TRAP", 0.51f} 
    };

    public GoapAction currentGoapAction;

    // Start is called before you know it.
    void Start()
    {
        name = "WOLF " + count;                                 // Names the Wolf.
        count++;                                                // Increases the counter of Wolves.
        WorldHandler.wolves.Add(this.gameObject);               // Adds to list of all Wolves.
        timerThreshold = Random.Range(9, 14);                   // Randomizes time between moves.

        wolfTint =                                              // Creates a random color for the Wolf.
            new Color(Random.Range(0.1f, 1),
            Random.Range(0.1f, 1),
            Random.Range(0.1f, 1), 1);

        GetComponent<SpriteRenderer>().color = wolfTint;        // Random tint for each Wolf (for fun!).

        currentPosition = transform.position;                   // Keeps track of the current position.
        nextPosition = transform.position;                      // Initializes next  position as current.
        destination = transform.position;                       // Initializes final position as current.

        hp = Random.Range(1, hpThreshold);                      // Randomizes starting health.

        foreach (GameObject tile in TileHandler.tileGrid)       // Instantiates all of the Tiles
        {                                                       //  with a corresponding TileNode.
            wolfDistances.Add(tile, new TileNode());            // Makes a new TileNode instance.
        }
    }

    // Update is called once in a while.
    void Update()
    {
        Autocorrect();                                          // Adjusts position if necessary.

        if (!searching)                                         // If not searching for path.
        {
            if (wolfPath.Count > 0)                             // If there is a path, ...
            {
                if (Vector3.Distance(transform.position,        // If not at destination, ...
                    wolfPath[wolfPath.Count - 1].transform.position) > 0.1f)
                {
                    transform.position = Vector3.Lerp(          // Move the Wolf toward the
                        transform.position,                     //  current waypoint.
                        wolfPath[wolfPath.Count - 1].transform.position,
                        0.05f * speed);
                }
                else
                {
                    currentPosition =                           // Set currentPos to Tile's Vector3.
                        wolfPath[wolfPath.Count - 1].transform.position;

                    crumbs.Add(wolfPath[wolfPath.Count - 1]);   // Add tile to crumbs for colorization.
                    transform.position = currentPosition;       // Make sure position is (_.5, _.5, 0).
                    wolfPath.Remove(                            // Remove tile from end of pathway list.
                        wolfPath[wolfPath.Count - 1]); 

                    foreach (GameObject g in crumbs)            // Set color of all tiles in crumbs list.
                    {
                        g.GetComponent<TileScript>().DecreaseAlpha(crumbs.IndexOf(g));
                    }
                    AdjustHealth();                             // Adjust the Wolf's health after movement.
                }
            }
            else
            {
                if (moving)
                {
                    Act();                                      // Perform designated action once.
                    moving = false;                             // Set moving to false if not moving.
                }
            }

            if (timer < timerThreshold)
            {
                timer += Time.deltaTime;                        // Increase timer.
            }
            else
            {
                MakeDecision();                                 // Call decision-making method.
            }
        }
    }

    void Autocorrect()                                          // Normalizes position.
    {
        if (currentPosition.x % 0.5f != 0 ||                    // Handles cases where the
            currentPosition.y % 0.5f != 0)                      //  currentPos is not _.5f.
        {
            currentPosition =
                new Vector3(Mathf.Round(currentPosition.x) + offset,
                Mathf.Round(currentPosition.y) + offset, currentPosition.z);
        }

        if (nextPosition.x % 0.5f != 0 ||                       // Handles cases where the
            nextPosition.y % 0.5f != 0)                         //  nextPos is not _.5f.
        {
            nextPosition =
                new Vector3(Mathf.Round(nextPosition.x) + offset,
                Mathf.Round(nextPosition.y) + offset, currentPosition.z);
        }

        if (transform.position.x < min_X)                       // Handles cases where the
        {                                                       //  sheep wander off the map.
            transform.position =
                new Vector3(min_X, transform.position.y, 0);
        }

        if (transform.position.x > max_X)
        {
            transform.position =
                new Vector3(max_X, transform.position.y, 0);
        }

        if (transform.position.y < min_Y)
        {
            transform.position =
                new Vector3(transform.position.x, min_Y, 0);
        }

        if (transform.position.y > max_Y)
        {
            transform.position =
                new Vector3(transform.position.x, max_Y, 0);
        }
    }

    void Act()                                                  // Called when a Wolf acts.
    {
        switch (currentAction)                                  // Determines current action.
        {
            case WolfActions.Absorbing:
                AbsorbWater();                                  // Absorbs water if on a water tile.
                break;
            case WolfActions.Splitting:                         
                MakeWolf();                                     // Creates a new Wolf.        
                break;
            case WolfActions.Trapping:
                SetTrap();                                      // Sets trap to hurt and/or slow 
                break;                                          //  down Sheep.
            default:
                break;
        }
        currentAction = WolfActions.Thinking;                   // Resets currentAction once done.
    }

    private void AbsorbWater()                                  // Method for destroying water.
    {
        TileScript here =                                       // Determines tile currently on.
            TileHandler.tileGrid[
                (int)(currentPosition.x - offset),
                (int)(currentPosition.y - offset)].GetComponent<TileScript>();
        Debug.Log(name + " is absorbing water at " + here.name + ".");
        here.DecreaseWater();                                   // Decreases water on this tile.
    }

    private void SetTrap()                                      // Method for setting traps.
    {
        TileScript here =                                       // Determines tile currently on.
            TileHandler.tileGrid[
                (int)(currentPosition.x - offset),
                (int)(currentPosition.y - offset)].GetComponent<TileScript>();
        Debug.Log(name + " is setting a trap at " + here.name + ".");
        if(here.terrainType == 13)                              // If an unset trap, ...
        {
            here.ChangeTile(14);                                // Changes to spike tile.
        }
    }

    void MakeDecision()                                         // Method for making decisions.
    {
        timer = 0;                                              // Resets timer.
        if (!moving)                                            // If not moving, ...
        {
            searching = true;                                   // Sets searching.

            GameObject here = TileHandler.tileGrid[             // Sets starting / current tile.
                (int)(currentPosition.x - offset),
                (int)(currentPosition.y - offset)].gameObject;

            GameObject there = TileHandler.tileGrid[            // Instantiates destination tile.
                (int)(currentPosition.x - offset),
                (int)(currentPosition.y - offset)].gameObject;

            float yum =                                         // Finds distance to closest Sheep.
                here.GetComponent<TileScript>().GetSheepDistances();

            string currentDecision = "";                        // Instantiates currentDecision.

            wolfChoices["ATTACK"] =                             // Calculates values for utility AI.
                (1 / (1 + Mathf.Pow(2.718f, (2 * (17.5f - yum - 8.5f)))));
            wolfChoices["DRINK"] =
                (1 / (1 + Mathf.Pow(2.718f, (-1 * (hp - 5f)))));
            wolfChoices["SPLIT"] = 
                (1 / (1 + Mathf.Pow(2.718f, (hp - 6f))));

            foreach(KeyValuePair<string, float> kvp in wolfChoices)
            {                                                   // Determines best choice (lowest).
                if (currentDecision.Equals(""))                 // If no current choice, sets first
                {                                               //  one to be the current choice.
                    currentDecision = kvp.Key;
                }
                else
                {
                    if (kvp.Value < wolfChoices[currentDecision])
                    {
                        currentDecision = kvp.Key;              // If less than current, replace.
                    }
                }
            }

            switch (currentDecision)                            // Determines pathfinding based on
            {                                                   //  best choice of action.
                case "ATTACK":                                  
                    there = FindSheep();                        //  Finds closest Sheep.
                    currentAction = WolfActions.Attacking;
                    break;
                case "DRINK":                           
                    there = FindWater();                        // Finds closest / best water tile.
                    currentAction = WolfActions.Absorbing;
                    break;
                case "SPLIT":
                    currentAction = WolfActions.Splitting;      // Creates a new Wolf.
                    break;
                case "TRAP":
                    there = FindTraps();                        // Finds nearest trap to set.
                    currentAction = WolfActions.Trapping;
                    break;
                default:
                    there =                                     // Finds random tile to move to.
                        TileHandler.tileGrid[
                            Random.Range((int)(min_X - offset), (int)(max_X - offset)),
                            Random.Range((int)(min_Y - offset), (int)(max_Y - offset))].gameObject;
                    currentAction = WolfActions.Thinking;
                    break;
            }
            PathfindingWithAStar(here, there);                  // Begins pathfinding.
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Sheep")                 // If colliding with a Sheep, ...
        {
            Debug.Log(name + " attacked " + collision.gameObject.name + " at " + currentPosition + ".");
            collision.gameObject.GetComponent<                  // Sheep takes damage.
                SheepScript>().TakeDamage(1);
            hp += 2;                                            // Wolf gains health.

            if(hp > MAX_HP)                                     // Caps at MAX_HP value.
            {
                hp = MAX_HP;
            }
        }
    }

    public void PathfindingWithAStar(GameObject startTile, GameObject endTile)
    {                                                           // A* Pathfinding!
        bool goalFound = false;                                 // Boolean local to this iteration.

        if (!moving)                                            // Does not compute if moving (no
        {                                                       //  chance of double click).                        
            openList.Clear();                                   // Clear all of the lists &
            wolfPath.Clear();                                   //  dictionaries for this
            closed.Clear();                                     //  Wolf.

            foreach (GameObject g in crumbs)
            {
                g.GetComponent<TileScript>().                   // Resets color of tiles.
                    SetTileColor(new Color(1, 1, 1, 0));
            }
            crumbs.Clear();                                     // Clears crumbs list.

            foreach (GameObject tile in TileHandler.tileGrid)   // Updates all of the data for each 
            {                                                   //  tiles' corresponding TileNode.
                if (!wolfDistances.ContainsKey(tile))
                {
                    wolfDistances.Add(tile, new TileNode());    // Makes a new TileNode if necessary.
                }

                wolfDistances[tile].gCost = int.MaxValue;       // Sets cost to "infinity".

                wolfDistances[tile].hCost =                     // Calculates distance from goal.
                    Vector2.Distance(tile.transform.position,
                    endTile.transform.position);

                wolfDistances[tile].CalculateFCost();           // Calculates total cost.

                wolfDistances[tile].parentNode = null;          // Makes parent node 'null' to start.
            }

            TileNode currentNode = wolfDistances[startTile];    // Makes startTile's TileNode currentNode.
            currentNode.gCost =                                 // Sets travel cost to (almost) zero.
                (startTile.GetComponent<TileScript>().cost / 31f);

            currentNode.hCost =                                 // Calculates distance to destination.
                Vector2.Distance(startTile.transform.position,
                endTile.transform.position);

            currentNode.CalculateFCost();                       // Calculates total cost.

            GameObject currentTile = startTile;                 // Instantiates currentTile as startTile.
            openList.Add(currentTile);                          // Adds current tile to the open list.

            while (openList.Count > 0)
            {
                //Debug.Log("OPENLIST.COUNT: " + openList.Count);

                GameObject lowestCost = null;                   // Instantiates lowest-cost tile.

                foreach (GameObject g in openList)              // Finds lowest-cost tile in open list.
                {
                    wolfDistances[g].CalculateFCost();          // Recalculates total cost (just in case).

                    if (lowestCost != null)                     // If there's already a lowest-cost tile,
                    {                                           //  determine if this tile's cost is lower.
                        if (wolfDistances[g].fCost < wolfDistances[lowestCost].fCost)
                        {
                            lowestCost = g;                     // Make this tile the lowest-cost tile.
                        }
                    }
                    else
                    {
                        lowestCost = g;                         // If no lowest-cost tile, make this one it.
                    }
                }
                currentTile = lowestCost;                       // Make current tile the lowest cost one.
                openList.Remove(currentTile);                   // Remove current tile from open list.
                //Debug.Log("CURRENT TILE: " + currentTile);

                foreach (GameObject adj in                      // Check all adjacent tiles.
                    currentTile.GetComponent<
                        TileScript>().adjacentTiles)
                {
                    if (!closed.Contains(adj))                  // Make sure this tile isn't already used
                    {                                           //  (stops stack overflow parenting loop).
                        wolfDistances[adj].parentNode =         // Make current tile the adjacent's parent.
                            currentTile;
                    }

                    if (adj == endTile)                         // Determines if we have found our end tile.
                    {
                        goalFound = true;
                    }

                    wolfDistances[adj].gCost =                  // Sets the cost to be current cost + half
                        wolfDistances[currentTile].gCost +      //  terrain costs divided by 20.
                        (adj.GetComponent<TileScript>().cost / 31f);
                    //Debug.Log(adj + " - G: " + distances[adj].gCost);

                    wolfDistances[adj].hCost =                  // Determines distance from goal.
                        Vector2.Distance(adj.transform.position,
                        endTile.transform.position);
                    //Debug.Log(adj + " - H: " + distances[adj].hCost);

                    wolfDistances[adj].CalculateFCost();        // Calculates total cost.

                    bool inadequate = false;                    // True if this tile is not optimal.

                    foreach (GameObject candidate in openList)
                    {
                        wolfDistances[candidate].CalculateFCost();  // Recalculate total cost.

                        if (wolfDistances[candidate].fCost < wolfDistances[adj].fCost)
                        {
                            inadequate = true;                  // A better candidate exists.
                        }
                    }
                    foreach (GameObject candidate in closed)
                    {
                        wolfDistances[candidate].CalculateFCost();  // Recalculate total cost.

                        if (wolfDistances[candidate].fCost < wolfDistances[adj].fCost)
                        {
                            inadequate = true;                  // A better path exists.
                        }
                    }

                    if(adj.GetComponent<TileScript>().isMine ||
                        adj.GetComponent<TileScript>().isHouse)
                    {
                        inadequate = true;
                    }

                    if (!inadequate && !openList.Contains(adj))
                    {
                        openList.Add(adj);                      // If best candidate, add to open list.
                    }
                }
                closed.Add(currentTile);                        // Add current tile to closed list.
            }
            if (goalFound)                                      // If we have found the goal,
            {                                                   //  we retrace our steps to
               // Debug.Log("GOAL FOUND!");                     //  find the path.
                Retrace(endTile);
            }
            else
            {                                                   // If the goal was not found,
                Debug.Log("!!! NO GOAL FOUND !!!");             //  retrace from where we
                Retrace(currentTile);                           //  currently are.
            }
        }
    }

    private void Retrace(GameObject nextTile)                   // Method for retracing steps.
    {
        if (wolfDistances[nextTile].parentNode != null)         // If this is not the starting tile:
        {
            //Debug.Log("Retracing from " + nextTile + " back to " + distances[nextTile].parentNode);
            wolfPath.Add(nextTile);                             // Add this tile to our path.
            nextTile.GetComponent<                              // Change color of tile (for fun!).
                TileScript>().SetTileColor(wolfTint);
            Retrace(wolfDistances[nextTile].parentNode);        // Run method from tile's parent.

        }
        else
        {                                                       // If this is the starting tile,
            moving = true;                                      //  set the two booleans for the
            searching = false;                                  //  Update method.
            //Debug.Log("Retrace complete. Starting movement!");
        }
    }

    private GameObject FindSheep()
    {
        //Debug.Log("FINDING SHEEP");
        GameObject bestFrenemy  = null;                         // Instantiate bestFrenemy  GameObject.
        GameObject closestSheep = null;                         // Instantiate closestSheep GameObject.

        foreach (GameObject s in WorldHandler.sheeps)           // Checks all Sheep in Gridworld.
        {
            if (closestSheep == null)   
            {
                closestSheep = s;                               // Sets closestSheep if null.
            }
            else
            {
                if (Vector2.Distance(this.transform.position,   // Finds closest Sheep.
                    s.transform.position) > Vector2.Distance(
                        this.transform.position, closestSheep.transform.position))
                {
                    closestSheep = s;                           // Sets closest Sheep.
                }
            }
        }

        if(closestSheep != null)
        {
            bestFrenemy = TileHandler.tileGrid[                 // Finds closest Sheep's tile.
                (int)closestSheep.GetComponent<SheepScript>().currentPos.x,
                (int)closestSheep.GetComponent<SheepScript>().currentPos.y].gameObject;
        }
        else
        {
            bestFrenemy = TileHandler.tileGrid[                 // Finds random Tile if no Sheep present.
                (int)(Random.Range((int)min_X, (int)max_X)),
                (int)(Random.Range((int)min_Y, (int)max_Y))].gameObject;
        }

        return bestFrenemy;                                     // Returns the best destination.
    }

    private GameObject FindWater()
    {
        //Debug.Log("FINDING WATER");   
        GameObject bestWater = null;                            // Instantiate bestWater GameObject

        foreach (KeyValuePair<GameObject,                       // Search through all tiles/nodes.
            TileNode> t in wolfDistances)
        {
            TileScript ts =                                     // Grab corresponding TileScript.
                t.Key.GetComponent<TileScript>();
            TileNode tn = t.Value;                              // Grab corresponding TileNode.

            if (ts.terrainType == 10 ||                         // If this Tile is a Water Tile.
                ts.terrainType == 11)
            {
                if (ts.occupied <= 0)                           // Makes sure it is not occupied.
                {
                    tn.tileInfo["WATER"] =                      // Calculates distance & length.
                        (ts.terrainType * -0.1f) + Vector2.Distance(
                            t.Key.transform.position, transform.position);

                    if (bestWater == null)                      // Determines bestWater based on
                    {                                           //  distance & level of water.
                        bestWater = t.Key;
                    }
                    else
                    {
                        if (tn.tileInfo["WATER"] <
                            wolfDistances[bestWater].tileInfo["WATER"])
                        {
                            bestWater = t.Key;
                        }
                    }
                }
            }
        }
        if(bestWater == null)
        {
            bestWater = TileHandler.tileGrid[                   // Finds random Tile if no water present.
                (int)(Random.Range((int)min_X, (int)max_X)),
                (int)(Random.Range((int)min_Y, (int)max_Y))].gameObject;
        }
        return bestWater;                                       // Returns best water tile.
    }

    private GameObject FindTraps()
    {
        //Debug.Log("FINDING TRAPS");
        GameObject bestTrap = null;                             // Instantiate bestWater GameObject

        foreach (KeyValuePair<GameObject,                       // Search through all tiles/nodes.
            TileNode> t in wolfDistances)
        {
            TileScript ts =                                     // Grab corresponding TileScript.
                t.Key.GetComponent<TileScript>();
            TileNode tn = t.Value;                              // Grab corresponding TileNode.

            if (ts.terrainType == 13)
            {
                if (ts.occupied <= 0)                           // Makes sure it is not occupied.
                {
                    tn.tileInfo["TRAPS"] =                      // Calculates distance & length.
                        (ts.terrainType * -0.1f) + Vector2.Distance(
                            t.Key.transform.position, transform.position);

                    if (bestTrap == null)                   
                    {                                           // Sets best tile if null.  
                        bestTrap = t.Key;                     
                    }
                    else
                    {
                        if (tn.tileInfo["TRAPS"] <              // Determines closest trap.
                            wolfDistances[bestTrap].tileInfo["TRAPS"])
                        {
                            bestTrap = t.Key;                   // Sets best tile.
                        }
                    }
                }
            }
        }
        if (bestTrap == null)
        {
            bestTrap = TileHandler.tileGrid[                    // Finds random Tile if no traps present.
                (int)(Random.Range((int)min_X, (int)max_X)),
                (int)(Random.Range((int)min_Y, (int)max_Y))].gameObject;
        }
        return bestTrap;                                        // Returns best tile.
    }

    public void MakeWolf()                                      // Method for creating a new Wolf.
    {
        GameObject baby = Instantiate(wolfPrefab,               // Instantiate new Wolf.
            currentPosition, Quaternion.identity);
        Debug.Log("Congratulations " + name + ", the proud parent of " + baby.name + " at " + currentPosition + ".");
        baby.GetComponent<WolfScript>().hp = hp * 0.5f;         // Splits this Wolf's health evenly
        this.hp = hp * 0.5f;                                    //  between this Wolf & the new Wolf.
    }

    public void AdjustHealth()                                  // Method for adjusting health.
    {
        hp -= 0.1f;                                             // Decrease health after movement.

        if(hp <= 0)                                             // If health depleated, ...
        {
            TileScript puddle = TileHandler.tileGrid[           // Determine currently occupied tile.
                (int)(currentPosition.x - offset),
                (int)(currentPosition.y - offset)].GetComponent<TileScript>();
            Debug.Log("Farewell, " + name + ". (@ " + puddle.name + ".");
            if (puddle.terrainType == 10)                       // If a Water (Lv. 1) tile, increase
            {                                                   //  to a Water (Lv. 2) tile.
                puddle.ChangeTile(11);
            }
            else if (puddle.terrainType == 11)                  // If a Water (Lv. 2) tile, make all
            {                                                   //  surrounding tiles Grass (Lv. 5) tiles.
                foreach(GameObject g in puddle.adjacentTiles)
                {
                    g.GetComponent<TileScript>().ChangeTile(5);
                }
            }
            else
            {
                puddle.ChangeTile(10);                          // Otherwise, make tile a Water (Lv. 1) tile.
            }
            foreach (GameObject g in crumbs)
            {
                g.GetComponent<TileScript>().                   // Resets color of tiles.
                    SetTileColor(new Color(1, 1, 1, 0));
            }
            crumbs.Clear();                                     // Clears crumbs list.
            WorldHandler.wolves.Remove(this.gameObject);        // Remove from list of Wolves.
            Destroy(this.gameObject);                           // Destroy this Wolf GameObject.
        }
    }
}
