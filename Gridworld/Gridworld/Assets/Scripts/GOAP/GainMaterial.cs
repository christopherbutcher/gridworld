﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainMaterial : GoapAction
{
    public Dictionary<string, bool> preconditions = new Dictionary<string, bool>()
    {
        {"MATERIAL", false}, {"MINE", true}, {"PICK", true}
    };

    public Dictionary<string, bool> postconditions = new Dictionary<string, bool>()
    {
        {"MATERIAL", true}
    };

    GameObject bestMine = null;

    public override GameObject FindLocationForGOAP(GameObject currentTile)
    {
        Debug.Log("GOAP!: Searching for a Mine to get material...");
        foreach(Mine m in WorldHandler.mines)
        {
            if(bestMine == null)
            {
                bestMine = m.gameObject;
            }
            else
            {
                if (Vector2.Distance(m.gameObject.transform.position, currentTile.transform.position) <
                    Vector2.Distance(bestMine.transform.position, currentTile.transform.position))
                {
                    bestMine = m.gameObject;
                }
            }
        }
        return bestMine;
    }

    public override void PerformAction(SheepScript sheep)
    {
        if(bestMine != null)
        {
            Debug.Log("GOAP!: " + sheep.name + " is mining at " + bestMine.name + ".");
            bestMine.GetComponent<Mine>().MineForResources();
            sheep.GOAPforSHEEP = new Dictionary<string, bool>(AdjustConditions(sheep.GOAPforSHEEP));
        }
        bestMine = null;
    }

    public override void PerformAction(WolfScript wolf)
    {
        throw new System.NotImplementedException();
    }

    public override Dictionary<string, bool> AdjustConditions(Dictionary<string, bool> dict)                                 //
    {
        Dictionary<string, bool> tempDictionary =
            new Dictionary<string, bool>(WorldHandler.worldConditions);
        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != tempDictionary[kvp.Key])           //
                {
                    Debug.Log("GOAP!: Updating World Conditions.");
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                          //
                }
                else
                {
                    //Debug.Log("GOAP!: " + postconditions[kvp.Key] + " (post).");
                    //Debug.Log("GOAP!: " + WorldHandler.worldConditions[kvp.Key] + " (world).");
                }
            }
        }

        while (WorldHandler.checkingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.updatingConditions = true;
        WorldHandler.worldConditions = new Dictionary<string, bool>(tempDictionary);
        WorldHandler.updatingConditions = false;

        tempDictionary.Clear();
        tempDictionary = new Dictionary<string, bool>(dict);

        foreach (KeyValuePair<string, bool> kvp in dict)                                        //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != dict[kvp.Key])                                   //
                {
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                                    //
                }
            }
        }
        return tempDictionary;
    }

    public override bool isPossible(Dictionary<string, bool> conditions)                                 //
    {
        bool result = true;                                                                     //

        while (WorldHandler.updatingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.checkingConditions = true;

        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                 //
        {
            if (preconditions.ContainsKey(kvp.Key.ToString()))                                  //
            {
                if (preconditions[kvp.Key] != WorldHandler.worldConditions[kvp.Key])             //
                {
                    result = false;                                                             //
                }
            }
        }

        WorldHandler.checkingConditions = false;

        foreach (KeyValuePair<string, bool> kvp in conditions)                                  //
        {
            //Debug.Log("GOAP!!!!: " + kvp.Key + " " + kvp.Value);
            if (preconditions.ContainsKey(kvp.Key))                                             //
            {
                if (preconditions[kvp.Key] != conditions[kvp.Key])                              //
                {
                    result = false;                                                             //
                }
            }
        }
        return result;                                                                          //
    }
}
