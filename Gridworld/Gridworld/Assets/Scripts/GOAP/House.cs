﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    public List<GameObject> cornerstones = new List<GameObject>();  // A list of all spaces this House is using.
    public List<Sprite> legos = new List<Sprite>();                 // A list of the sprites used for each House.
    public int currentHouseChunk = 0;                               // How many House chunks have been built.
    public int occupancy = 0;                                       // How many Sheep are currently in the House.
    public static int MAX_OCCUPANCY = 4;                            // Maximum number of Sheep able to be in House.
    static int houseNum = 0;                                        // Number of Houses made (for naming).

    private void Start()
    {
        name = "HOUSE " + houseNum;
        houseNum++;
        WorldHandler.houses.Add(this);
        WorldHandler.worldConditions["FULL"] = false;
        GameObject homebase = 
            TileHandler.tileGrid[
                (int)transform.position.x, 
                (int)transform.position.y];
        cornerstones.Add(homebase);
        foreach(GameObject adj in 
            homebase.GetComponent<TileScript>().adjacentTiles)
        {
            cornerstones.Add(adj);
        }
        OrganizeCornerstones();
        Debug.Log("GOAP!: House has been created at " + homebase.name + ".");
    }

    public void OrganizeCornerstones()                              // Sorts the House's nine sections by location.
    {
        cornerstones.Sort((c1,c2)=>c1.transform.position.x.CompareTo(c2.transform.position.x));
        cornerstones.Sort((c1,c2)=>c1.transform.position.y.CompareTo(c2.transform.position.y));

        foreach(GameObject tile in cornerstones)
        {
            tile.GetComponent<TileScript>().isHouse = true;
        }
    }

    public void BuildHouseChunk(GameObject tile)
    {
        Debug.Log("GOAP!: Building chunk " + currentHouseChunk + " at " + tile.name + ".");
        if (cornerstones.Contains(tile))
        {
            int chunk = cornerstones.IndexOf(tile);
            tile.GetComponent<TileScript>().ChangeTile(9);
            tile.GetComponent<SpriteRenderer>().sprite = legos[chunk];
            tile.GetComponent<SpriteRenderer>().color = Color.white;
            currentHouseChunk++;
        }
        else
        {
            Debug.LogError("GOAP!: Bad bones!");
        }

        if(currentHouseChunk > 8)
        {
            WorldHandler.worldConditions["BUILD"] = false;
            WorldHandler.worldConditions["HOME" ] =  true;
        }
    }

    public void Demolish(GameObject tile)
    {
        if (cornerstones.Contains(tile))
        {
            int chunk = cornerstones.IndexOf(tile);
            tile.GetComponent<TileScript>().ChangeTile(0);
            tile.GetComponent<SpriteRenderer>().sprite = null;
            currentHouseChunk--;
        }
        else
        {
            Debug.LogError("GOAP!: There hasn't been a house here since 199X!");
        }
    }

    public void DetermineOccupancy()
    {
        Debug.Log("GOAP!: Anybody home?");
        occupancy++;
        if(occupancy >= MAX_OCCUPANCY)
        {
            Debug.Log("GOAP!: House at " + cornerstones[5].name + " is full!");
            WorldHandler.worldConditions["FULL"] = true;
            WorldHandler.worldConditions["HOME"] = true;
        }
    }
}
