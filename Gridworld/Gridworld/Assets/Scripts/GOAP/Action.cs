﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Action", menuName="ScriptableObjects/Action")]
public class Action : ScriptableObject
{
    public bool   condition;
    public Action previousAction;
    public Action nextAction;
    public Action goalAction;

    public void PerformAction(int greaterThan, int lessThan)
    {
        
    }

    public void PerformAction(Action a)
    {

    }
}
