﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeSecure : GoapAction
{
    public Dictionary<string, bool> preconditions = new Dictionary<string, bool>()
    {
        { "HOME", true }, {"FULL", false}, {"BUILD", false}
    };

    public Dictionary<string, bool> postconditions = new Dictionary<string, bool>()
    {
        { "SAFE", true }
    };

    House currentHouse = null;

    public override GameObject FindLocationForGOAP(GameObject currentTile)
    {
        Debug.Log("GOAP!: Finding security.");
        GameObject bestHouse = null;
        foreach(House h in WorldHandler.houses)
        {
            if(bestHouse == null && h.occupancy < House.MAX_OCCUPANCY)
            {
                bestHouse = h.cornerstones[1];
                currentHouse = h;
            }
            else
            {
                if(bestHouse != null)
                {
                    if (Vector2.Distance(currentTile.transform.position,
                    h.cornerstones[1].transform.position) < Vector2.Distance(
                        currentTile.transform.position, bestHouse.transform.position)
                    && h.occupancy < House.MAX_OCCUPANCY)
                    {
                        bestHouse = h.cornerstones[1];
                        currentHouse = h;
                    }
                }
            }
        }
        return bestHouse;
    }

    public override void PerformAction(SheepScript sheep)
    {
        sheep.GOAPforSHEEP = new Dictionary<string, bool>(AdjustConditions(sheep.GOAPforSHEEP));
        if (currentHouse != null)
        {
            if(currentHouse.occupancy < House.MAX_OCCUPANCY 
                && currentHouse.currentHouseChunk == 9)
            {
                Debug.Log("GOAP!: " + sheep.name + " has found a home at " + currentHouse.name + "!");
                currentHouse.DetermineOccupancy();
                sheep.GoHome();
            }
        }
        else
        {
            Debug.Log("GOAP!: THIS HOUSE IS NOT A HOME, " + sheep.name + ".");
        }
        currentHouse = null;
    }

    public override void PerformAction(WolfScript wolf)
    {
        throw new System.NotImplementedException();
        //AdjustConditions(wolf.GOAPforWOLVES);
    }

    public override Dictionary<string, bool> AdjustConditions(Dictionary<string, bool> dict)                                 //
    {
        Dictionary<string, bool> tempDictionary =
            new Dictionary<string, bool>(WorldHandler.worldConditions);
        //Debug.Log("^^^^ temp: " + tempDictionary.Count + " dict: " + dict.Count);
        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != tempDictionary[kvp.Key])           //
                {
                    Debug.Log("GOAP!: Updating World Conditions.");
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                          //
                }
                else
                {
                    //Debug.Log("GOAP!: " + postconditions[kvp.Key] + " (post).");
                    //Debug.Log("GOAP!: " + WorldHandler.worldConditions[kvp.Key] + " (world).");
                }
            }
        }

        while (WorldHandler.checkingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.updatingConditions = true;
        WorldHandler.worldConditions = new Dictionary<string, bool>(tempDictionary);
        WorldHandler.updatingConditions = false;

        tempDictionary.Clear();
        tempDictionary = new Dictionary<string, bool>(dict);

        foreach (KeyValuePair<string, bool> kvp in dict)                                        //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != dict[kvp.Key])                                   //
                {
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                                    //
                }
            }
        }
        return tempDictionary;
    }

    public override bool isPossible(Dictionary<string, bool> conditions)                                 //
    {
        bool result = true;                                                                     //

        while (WorldHandler.updatingConditions)
        {
            Debug.Log("GOAP!: Waiting on World Conditions...");
            new WaitForSeconds(1);
        }

        WorldHandler.checkingConditions = true;
        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                 //
        {
            if (preconditions.ContainsKey(kvp.Key.ToString()))                                  //
            {
                if (preconditions[kvp.Key] != WorldHandler.worldConditions[kvp.Key])             //
                {
                    result = false;                                                             //
                }
            }
        }

        WorldHandler.checkingConditions = false;

        foreach (KeyValuePair<string, bool> kvp in conditions)                                  //
        {
            //Debug.Log("GOAP!!!!: " + kvp.Key + " " + kvp.Value);
            if (preconditions.ContainsKey(kvp.Key))                                             //
            {
                if (preconditions[kvp.Key] != conditions[kvp.Key])                              //
                {
                    result = false;                                                             //
                }
            }
        }
        //Debug.Log("*GOAP: RESULT:::" + result);
        return result;                                                                          //
    }
}
