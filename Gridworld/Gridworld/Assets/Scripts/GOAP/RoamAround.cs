﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoamAround : GoapAction
{
    public Dictionary<string, bool>  preconditions = new Dictionary<string, bool>();
    public Dictionary<string, bool> postconditions = new Dictionary<string, bool>();

    public override GameObject FindLocationForGOAP(GameObject currentTile)
    {
        return TileHandler.tileGrid[
                            Random.Range((int)(SheepScript.min_X - SheepScript.offset), 
                            (int)(SheepScript.max_X - SheepScript.offset)), Random.Range(
                                (int)(SheepScript.min_Y - SheepScript.offset), 
                                (int)(SheepScript.max_Y - SheepScript.offset))].gameObject;
    }

    public override void PerformAction(SheepScript sheep)
    {
        Debug.Log("GOAP!: " + sheep.name + " has gone for a walk!");
    }

    public override void PerformAction(WolfScript wolf)
    {
        throw new System.NotImplementedException();
    }

    public override Dictionary<string, bool> AdjustConditions(Dictionary<string, bool> dict)                                 //
    {
        Dictionary<string, bool> tempDictionary =
            new Dictionary<string, bool>(WorldHandler.worldConditions);
        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != tempDictionary[kvp.Key])           //
                {
                    Debug.Log("GOAP!: Updating World Conditions.");
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                          //
                }
                else
                {
                    //Debug.Log("GOAP!: " + postconditions[kvp.Key] + " (post).");
                    //Debug.Log("GOAP!: " + WorldHandler.worldConditions[kvp.Key] + " (world).");
                }
            }
        }

        while (WorldHandler.checkingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.updatingConditions = true;
        WorldHandler.worldConditions = new Dictionary<string, bool>(tempDictionary);
        WorldHandler.updatingConditions = false;

        tempDictionary.Clear();
        tempDictionary = new Dictionary<string, bool>(dict);

        foreach (KeyValuePair<string, bool> kvp in dict)                                        //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != dict[kvp.Key])                                   //
                {
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                                    //
                }
            }
        }
        return tempDictionary;
    }

    public override bool isPossible(Dictionary<string, bool> conditions)                                 //
    {
        bool result = true;                                                                     //

        while (WorldHandler.updatingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.checkingConditions = true;

        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                 //
        {
            if (preconditions.ContainsKey(kvp.Key.ToString()))                                  //
            {
                if (preconditions[kvp.Key] != WorldHandler.worldConditions[kvp.Key])             //
                {
                    result = false;                                                             //
                }
            }
        }

        WorldHandler.checkingConditions = false;

        foreach (KeyValuePair<string, bool> kvp in conditions)                                  //
        {
            //Debug.Log("GOAP!!!!: " + kvp.Key + " " + kvp.Value);
            if (preconditions.ContainsKey(kvp.Key))                                             //
            {
                if (preconditions[kvp.Key] != conditions[kvp.Key])                              //
                {
                    result = false;                                                             //
                }
            }
        }
        return result;                                                                          //
    }
}
