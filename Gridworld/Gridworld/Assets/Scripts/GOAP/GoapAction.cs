﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GoapAction                                                                //
{
    Dictionary<string, bool>  preconditions = new Dictionary<string, bool>();                   //
    Dictionary<string, bool> postconditions = new Dictionary<string, bool>();                   //

    public abstract void PerformAction(SheepScript sheep);                                      //
    public abstract void PerformAction(WolfScript wolf);                                        //
    public abstract GameObject FindLocationForGOAP(GameObject currentTile);                     //

    public abstract Dictionary<string, bool> AdjustConditions(Dictionary<string, bool> dict);   //
    public abstract bool isPossible(Dictionary<string, bool> conditions);                       //
}
