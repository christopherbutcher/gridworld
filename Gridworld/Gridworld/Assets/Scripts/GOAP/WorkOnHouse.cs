﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkOnHouse : GoapAction
{
    public Dictionary<string, bool> preconditions = new Dictionary<string, bool>()
    {
        {"HAMMER", true }, {"MATERIAL", true}, {"BUILD", true}
    };

    public Dictionary<string, bool> postconditions = new Dictionary<string, bool>()
    {
        {"MATERIAL", false}
    };

    House currentHouse = null;

    public override GameObject FindLocationForGOAP(GameObject currentTile)
    {
        Debug.Log("GOAP!: Finding next House piece...");
        GameObject houseTile = null;

        foreach(House h in WorldHandler.houses)
        {
            if(h.currentHouseChunk <= 8)
            {
                if (currentHouse == null)
                {
                    currentHouse = h;
                }
            }
        }

        if(currentHouse != null)
        {
            houseTile = currentHouse.cornerstones[currentHouse.currentHouseChunk];
        }

        return houseTile;
    }

    public override void PerformAction(SheepScript sheep)
    {
        if(currentHouse != null)
        {
            Debug.Log("GOAP!: " + sheep.name + " is building a House at " + currentHouse.transform.position + ".");
            currentHouse.BuildHouseChunk(
                TileHandler.tileGrid[
                    (int)sheep.currentPos.x,
                    (int)sheep.currentPos.y]);
        }
        else
        {
            Debug.Log("GOAP!: " + sheep.name + " is building an imaginary House...");
        }
        sheep.GOAPforSHEEP["MATERIAL"] = false;
        currentHouse = null;
    }

    public override void PerformAction(WolfScript wolf)
    {
        throw new System.NotImplementedException();
    }

    public override Dictionary<string, bool> AdjustConditions(Dictionary<string, bool> dict)                                 //
    {
        Dictionary<string, bool> tempDictionary =
            new Dictionary<string, bool>(WorldHandler.worldConditions);
        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != tempDictionary[kvp.Key])           //
                {
                    Debug.Log("GOAP!: Updating World Conditions.");
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                          //
                }
                else
                {
                    //Debug.Log("GOAP!: " + postconditions[kvp.Key] + " (post).");
                    //Debug.Log("GOAP!: " + WorldHandler.worldConditions[kvp.Key] + " (world).");
                }
            }
        }

        while (WorldHandler.checkingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.updatingConditions = true;
        WorldHandler.worldConditions = new Dictionary<string, bool>(tempDictionary);
        WorldHandler.updatingConditions = false;

        tempDictionary.Clear();
        tempDictionary = new Dictionary<string, bool>(dict);

        foreach (KeyValuePair<string, bool> kvp in dict)                                        //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != dict[kvp.Key])                                   //
                {
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                                    //
                }
            }
        }
        return tempDictionary;
    }

    public override bool isPossible(Dictionary<string, bool> conditions)                                 //
    {
        bool result = true;                                                                     //

        while (WorldHandler.updatingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.checkingConditions = true;

        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                 //
        {
            if (preconditions.ContainsKey(kvp.Key.ToString()))                                  //
            {
                if (preconditions[kvp.Key] != WorldHandler.worldConditions[kvp.Key])             //
                {
                    result = false;                                                             //
                }
            }
        }

        WorldHandler.checkingConditions = false;

        foreach (KeyValuePair<string, bool> kvp in conditions)                                  //
        {
            //Debug.Log("GOAP!!!!: " + kvp.Key + " " + kvp.Value);
            if (preconditions.ContainsKey(kvp.Key))                                             //
            {
                if (preconditions[kvp.Key] != conditions[kvp.Key])                              //
                {
                    result = false;                                                             //
                }
            }
        }
        return result;                                                                          //
    }
}
