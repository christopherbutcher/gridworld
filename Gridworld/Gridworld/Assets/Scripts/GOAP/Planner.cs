﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planner : MonoBehaviour
{
    public GameObject minePrefab;
    public GameObject housePrefab;

    public List<GoapAction> sheepActions = new List<GoapAction>()
    {
        new BeSecure(),     new WorkOnHouse(),  new PourFoundation(), 
        new MakeHammer(),   new GainMaterial(), new BuildMine(),
        new MakePick()
    };

    //public List<GoapAction> wolfActions = new List<GoapAction>();

    public Dictionary<string, bool> sheepGoals = new Dictionary<string, bool>()
    {
        {"SAFE", true}
    };

    public GoapAction DetermineNextAction(SheepScript sheep)
    {
        GoapAction nextAction = null;
        foreach(GoapAction goap in sheepActions)
        {
            if (goap.isPossible(sheep.GOAPforSHEEP))
            {
                //Debug.Log("GOAP!: The next action for " + sheep.name + " is "+goap.ToString()+"!");
                nextAction = goap;
                return nextAction;
            }
        }
        return nextAction;
    }

    public void MakeMine(Vector3 position)
    {
        GameObject newMine = Instantiate(minePrefab, position, Quaternion.identity);
    }

    public void MakeHouse(Vector3 position)
    {
        GameObject newHouse = Instantiate(housePrefab, position, Quaternion.identity);
    }
}
