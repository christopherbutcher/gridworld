﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PourFoundation : GoapAction
{
    public Dictionary<string, bool> preconditions = new Dictionary<string, bool>()
    {
        {"HAMMER", true }, {"MATERIAL", true}
    };

    public Dictionary<string, bool> postconditions = new Dictionary<string, bool>()
    {
        {"BUILD", true}, {"MATERIAL", false}
    };

    public override GameObject FindLocationForGOAP(GameObject currentTile)
    {
        Debug.Log("GOAP!: Searching for a place to build a new House...");
        GameObject bestTile = null;

        foreach(GameObject tile in TileHandler.tileGrid)
        {
            bool badFoundation = false;

            if (tile.GetComponent<TileScript>().isMine ||
                tile.GetComponent<TileScript>().isHouse ||
                tile.GetComponent<TileScript>().occupied > 1)
            {
                badFoundation = true;
            }

            if (tile.GetComponent<TileScript>().adjacentTiles.Count == 8 && !badFoundation)
            {
                foreach (GameObject adj in tile.GetComponent<TileScript>().adjacentTiles)
                {
                    if (adj.GetComponent<TileScript>().isMine ||
                        adj.GetComponent<TileScript>().isHouse ||
                        adj.GetComponent<TileScript>().occupied > 1)
                    {
                        badFoundation = true;
                    }
                }
            }
            else
            {
                badFoundation = true;
            }

            if (!badFoundation)
            {
                if(bestTile == null)
                {
                    bestTile = tile;
                }
                else
                {
                    if(Vector2.Distance(
                        currentTile.transform.position, tile.transform.position) <
                        Vector2.Distance(currentTile.transform.position, 
                        bestTile.transform.position))
                    {
                        bestTile = tile;
                    }
                } 
            }
        }
        return bestTile;
    }

    public override void PerformAction(SheepScript sheep)
    {
        Debug.Log("GOAP!: " + sheep.name + " is pouring the foundation for a new House!");
        WorldHandler.plan.MakeHouse(sheep.currentPos);
        sheep.GOAPforSHEEP = new Dictionary<string, bool>(AdjustConditions(sheep.GOAPforSHEEP));

    }

    public override void PerformAction(WolfScript wolf)
    {
        throw new System.NotImplementedException();
    }

    public override Dictionary<string, bool> AdjustConditions(Dictionary<string, bool> dict)                                 //
    {
        Dictionary<string, bool> tempDictionary =
            new Dictionary<string, bool>(WorldHandler.worldConditions);
        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != tempDictionary[kvp.Key])           //
                {
                    Debug.Log("GOAP!: Updating World Conditions.");
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                          //
                }
                else
                {
                    //Debug.Log("GOAP!: " + postconditions[kvp.Key] + " (post).");
                    //Debug.Log("GOAP!: " + WorldHandler.worldConditions[kvp.Key] + " (world).");
                }
            }
        }

        while (WorldHandler.checkingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.updatingConditions = true;
        WorldHandler.worldConditions = new Dictionary<string, bool>(tempDictionary);
        WorldHandler.updatingConditions = false;

        tempDictionary.Clear();
        tempDictionary = new Dictionary<string, bool>(dict);

        foreach (KeyValuePair<string, bool> kvp in dict)                                        //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != dict[kvp.Key])                                   //
                {
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                                    //
                }
            }
        }
        return tempDictionary;
    }

    public override bool isPossible(Dictionary<string, bool> conditions)                                 //
    {
        bool result = true;                                                                     //

        while (WorldHandler.updatingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.checkingConditions = true;

        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                 //
        {
            if (preconditions.ContainsKey(kvp.Key.ToString()))                                  //
            {
                if (preconditions[kvp.Key] != WorldHandler.worldConditions[kvp.Key])             //
                {
                    result = false;                                                             //
                }
            }
        }

        WorldHandler.checkingConditions = false;

        foreach (KeyValuePair<string, bool> kvp in conditions)                                  //
        {
            //Debug.Log("GOAP!!!!: " + kvp.Key + " " + kvp.Value);
            if (preconditions.ContainsKey(kvp.Key))                                             //
            {
                if (preconditions[kvp.Key] != conditions[kvp.Key])                              //
                {
                    result = false;                                                             //
                }
            }
        }
        return result;                                                                          //
    }
}
