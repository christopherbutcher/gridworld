﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public GameObject mineLocation;
    public int uses;
    static int mineNum = 0;

    // Start is called before the first frame update
    void Start()
    {
        name = "MINE " + mineNum;
        mineNum++;
        uses = Random.Range(1, 10);
        mineLocation = TileHandler.tileGrid[
            (int)transform.position.x, (int)transform.position.y];
        mineLocation.GetComponent<TileScript>().ChangeTile(6);
        WorldHandler.mines.Add(this);
        Debug.Log("GOAP!: Mine created at " + mineLocation.name + ".");
    }

    public void MineForResources()
    {
        Debug.Log("GOAP!: Resources mined at " + mineLocation.name + ".");
        uses--;
        if(uses <= 0)
        {
            CollapseMine();
        }
    }

    public void CollapseMine()
    {
        Debug.Log("GOAP!: Mine collapse at " + mineLocation.name + "!");
        mineLocation.GetComponent<TileScript>().ChangeTile(12);
        mineLocation.GetComponent<TileScript>().isMine = false;
        WorldHandler.mines.Remove(this);
        WorldHandler.CheckWorldState();
        Destroy(this.gameObject);
    }
}
