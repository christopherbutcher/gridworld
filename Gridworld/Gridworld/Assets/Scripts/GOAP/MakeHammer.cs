﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeHammer : GoapAction
{
    public Dictionary<string, bool> preconditions = new Dictionary<string, bool>()
    {
        {"ROCKS", true}, {"HAMMER", false}
    };

    public Dictionary<string, bool> postconditions = new Dictionary<string, bool>()
    {
        {"HAMMER", true}
    };

    public override GameObject FindLocationForGOAP(GameObject currentTile)
    {
        Debug.Log("GOAP!: Searching for a rock to turn into a Hammer...");
        GameObject bestRock = null;
        foreach (GameObject g in TileHandler.tileGrid)
        {
            if (g.GetComponent<TileScript>().terrainType == 12)
            {
                if (bestRock == null)
                {
                    bestRock = g;
                }
                else
                {
                    if (Vector2.Distance(
                        currentTile.transform.position,
                        g.transform.position) <
                        Vector2.Distance(
                            currentTile.transform.position,
                            bestRock.transform.position))
                    {
                        bestRock = g;
                    }
                }
            }
        }
        return bestRock;
    }

    public override void PerformAction(SheepScript sheep)
    {
        GameObject tile = TileHandler.tileGrid[(int)sheep.currentPos.x, (int)sheep.currentPos.y];
        if (tile.GetComponent<TileScript>().terrainType == 12)
        {
            tile.GetComponent<TileScript>().ChangeTile(0);
            WorldHandler.CheckWorldState();
            sheep.GOAPforSHEEP = new Dictionary<string, bool>(AdjustConditions(sheep.GOAPforSHEEP));
            Debug.Log("GOAP!: " + sheep.name + " just broke a rock at " + tile.name + " to make a Hammer!");
        }
        else
        {
            Debug.Log("GOAP!: " + sheep.name + " found out " + tile.name + " is not a rock.");
        }

    }

    public override void PerformAction(WolfScript wolf)
    {
        throw new System.NotImplementedException();
    }

    public override Dictionary<string, bool> AdjustConditions(Dictionary<string, bool> dict)                                 //
    {
        Dictionary<string, bool> tempDictionary =
            new Dictionary<string, bool>(WorldHandler.worldConditions);
        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != tempDictionary[kvp.Key])           //
                {
                    Debug.Log("GOAP!: Updating World Conditions.");
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                          //
                }
                else
                {
                    //Debug.Log("GOAP!: " + postconditions[kvp.Key] + " (post).");
                    //Debug.Log("GOAP!: " + WorldHandler.worldConditions[kvp.Key] + " (world).");
                }
            }
        }

        while (WorldHandler.checkingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.updatingConditions = true;
        //Debug.Log("::: NON-UPDATED WORLD CONS:" + WorldHandler.worldConditions.Count);
        WorldHandler.worldConditions = new Dictionary<string, bool>(tempDictionary);
        //Debug.Log("::: UPDATED WORLD CONS:" + WorldHandler.worldConditions.Count);
        WorldHandler.updatingConditions = false;

        tempDictionary.Clear();
        tempDictionary = new Dictionary<string, bool>(dict);

        foreach (KeyValuePair<string, bool> kvp in dict)                                        //
        {
            if (postconditions.ContainsKey(kvp.Key))                                            //
            {
                if (postconditions[kvp.Key] != dict[kvp.Key])                                   //
                {
                    tempDictionary[kvp.Key] = postconditions[kvp.Key];                                    //
                }
            }
        }
        //Debug.Log("::: UPDATED WORLD CONS PT2:" + WorldHandler.worldConditions.Count);
        return tempDictionary;
    }

    public override bool isPossible(Dictionary<string, bool> conditions)                                 //
    {
        bool result = true;                                                                     //

        while (WorldHandler.updatingConditions)
        {
            new WaitForSeconds(1);
        }

        WorldHandler.checkingConditions = true;

        foreach (KeyValuePair<string, bool> kvp in WorldHandler.worldConditions)                 //
        {
            if (preconditions.ContainsKey(kvp.Key.ToString()))                                  //
            {
                if (preconditions[kvp.Key] != WorldHandler.worldConditions[kvp.Key])             //
                {
                    result = false;                                                             //
                }
            }
        }

        WorldHandler.checkingConditions = false;

        foreach (KeyValuePair<string, bool> kvp in conditions)                                  //
        {
            //Debug.Log("GOAP!!!!: " + kvp.Key + " " + kvp.Value);
            if (preconditions.ContainsKey(kvp.Key))                                             //
            {
                if (preconditions[kvp.Key] != conditions[kvp.Key])                              //
                {
                    result = false;                                                             //
                }
            }
        }
        return result;                                                                          //
    }
}
